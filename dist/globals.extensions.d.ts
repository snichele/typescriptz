import { api } from "./api/typescriptz-api";
export {};
declare global {
    interface String {
        just: () => api.Maybe<string>;
    }
    interface Boolean {
        just: () => api.Maybe<boolean>;
    }
    interface Number {
        just: () => api.Maybe<number>;
    }
    interface StringConstructor {
        fromNullable: (val: any) => api.Maybe<string>;
    }
    interface BooleanConstructor {
        fromNullable: (val: any) => api.Maybe<boolean>;
    }
    interface NumberConstructor {
        fromNullable: (val: any) => api.Maybe<number>;
    }
}
