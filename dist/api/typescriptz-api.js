"use strict";
/**
 * This file exposes an internal namespace 'api', that aggregates :
 *  - several ADT (algebraic data types) / Monad / abstraction interfaces for FP
 *  - some utility sub-modules, for ... hyper-generic utility functions !
 *
 * Look into the 'api' namespace below for details.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Exposes TypeScriptZ api through several interfaces and some utility modules.
 *
 * **Each interface** is supposed to be **related to a namespace with the same name**,
 * that exposes functions to instantiate concrete types extending them,
 * located into the **'Globals / core' package**.
 *
 * ie. api.Maybe interface → core.Maybe module
 *
 * <pre><code>
 *  import {api, Maybe} from "typescriptz";

 *  const aMaybe: api.Maybe<string> = Maybe.Just("foo");
 * </code></pre>
 *
 * Amongst the proposed interfaces, **you can begin discovering the api by looking
 * at the Maybe and Either ADT interfaces**.
 *
 * <span style="text-decoration:underline">The library is supposed to be extensively tested through several specs. Refer
 * to them for some really unit usages.</span>
 *
 */
var api;
(function (api) {
    var Predicates;
    (function (Predicates) {
        function nullOrEmptyStringPredicate(s) {
            return Predicates.nullOrEmptyPredicate(s) || s === "";
        }
        Predicates.nullOrEmptyStringPredicate = nullOrEmptyStringPredicate;
        function nullOrEmptyPredicate(x) {
            return x === null || x === undefined;
        }
        Predicates.nullOrEmptyPredicate = nullOrEmptyPredicate;
    })(Predicates = api.Predicates || (api.Predicates = {}));
})(api = exports.api || (exports.api = {}));
//# sourceMappingURL=typescriptz-api.js.map