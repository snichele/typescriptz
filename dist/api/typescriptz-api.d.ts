/**
 * This file exposes an internal namespace 'api', that aggregates :
 *  - several ADT (algebraic data types) / Monad / abstraction interfaces for FP
 *  - some utility sub-modules, for ... hyper-generic utility functions !
 *
 * Look into the 'api' namespace below for details.
 */
/**
 * Exposes TypeScriptZ api through several interfaces and some utility modules.
 *
 * **Each interface** is supposed to be **related to a namespace with the same name**,
 * that exposes functions to instantiate concrete types extending them,
 * located into the **'Globals / core' package**.
 *
 * ie. api.Maybe interface → core.Maybe module
 *
 * <pre><code>
 *  import {api, Maybe} from "typescriptz";

 *  const aMaybe: api.Maybe<string> = Maybe.Just("foo");
 * </code></pre>
 *
 * Amongst the proposed interfaces, **you can begin discovering the api by looking
 * at the Maybe and Either ADT interfaces**.
 *
 * <span style="text-decoration:underline">The library is supposed to be extensively tested through several specs. Refer
 * to them for some really unit usages.</span>
 *
 */
export declare namespace api {
    interface Context<T> {
    }
    /** I can put something in a context.
     *
     *  x + Context[ ... ] = Context[x]
     *
     * */
    interface Pointed<T> {
        point(a: () => T): Context<T>;
    }
    /**
     * I can execute some function on a value in a context.
     *
     *  f + Context[x] = Context[f(x)]
     *
     * */
    interface Functor<A> {
        fmap<B, C extends Context<A>, D extends Context<B>>(f: (a: A) => B): (c: C) => D;
    }
    /**
     * I can execute some function in a context to a value in a context.
     *
     *  Context[f] + Context[x] = Context[f(x)]
     *
     * */
    interface Applic {
        applic<A, B>(f: Context<(a: A) => B>): (ma: Context<A>) => Context<B>;
    }
    /**
     * Applicative Functor, described in [[http://www.soi.city.ac.uk/~ross/papers/Applicative.html Applicative Programming with Effects]]
     *
     * Whereas a [[api.Functor]] allows application of a pure function to a value in a context, an Applicative
     * also allows application of a function in a context to a value in a context (`ap`).
     * */
    interface Applicative<A> extends Functor<A>, Pointed<A>, Applic {
        ap<B>(f: api.Context<(a: A) => B>): (a: api.Context<A>) => api.Context<B>;
    }
    /**
     * `Maybe` represents an optional value (can contains a value, or not).
     *
     * An intantiated `Maybe<A>` will either be
     * - a wrapped `A` instance  => `Just<A>`
     * - or a lack of underlying `A` instance  => `Empty<A>`
     *
     * Sometimes called Option (Scala), Optional (Java8).
     *
     * ADT : A sum type.
     */
    interface Maybe<T> extends Context<T> {
        /**
         * Return true if this context
         * - contains a value
         * - the predicate applied to it returns true.
         *
         * @param predicate to test the value for truthness
         */
        exists(predicate: (t: T) => boolean): boolean;
        /**
         * Returns a Maybe.Empty() if this maybe is Empty or if the contained value does NOT match the predicate.
         * If this Maybe is a Maybe.Just() AND the underlying value match the predicate, then return the original Maybe.
         *
         * @param predicate A function that accepts up to three arguments.
         *                    The filter method calls the predicate function on the underlying value only if there is one.
         */
        filter(predicate: (value: T) => boolean): Maybe<T>;
        /**
         * The 'inverse' of the filter method : hold the original Maybe only if there is a content that does NOT match the predicate.
         * Otherwise, return the Maybe.Empty()
         *
         * @param predicate A function that accepts up to three arguments.
         *                    The filter method calls the predicate function on the underlying value only if there is one.
         */
        filterNot(predicate: (value: T) => boolean): Maybe<T>;
        /** Return the underlying value if present, otherwise the provided fallback value (as a thunk). */
        getOrElse(thunk: () => T): T;
        /** Return a Maybe with current value if it is not empty, otherwise return the other provided Maybe of the same type T. */
        orElse(m: Maybe<T>): Maybe<T>;
        /** Return the underlying value, 'void' if nothing is present. Unsafe (prefer getOrElse)*/
        get(): T | undefined;
        /** True if an underlying value is present */
        isJust(): boolean;
        /** True if an underlying value is present */
        isDefined(): boolean;
        /** True if no underlying value is present */
        isEmpty(): boolean;
        /** Apply the function in this context. */
        map<B>(f: (a: T) => B): Maybe<B>;
        /** Apply the function in a value already in a context and extract it. */
        flatMap<B>(f: (a: T) => Maybe<B>): Maybe<B>;
        /** Apply the function `f` in this context, returning the resulting value ; if there is no value, return
         * the default value provided by `isEmpty`.
         * */
        fold<B>(f: (a: T) => B, ifEmpty: () => B): B;
        asApplicative(): Applicative<T>;
        match<B>(m: MaybeMatcher<T, B>): B;
        /** Compare two maybes of the same nature, returning true only if both are empty
         * or the two contains the same value (comparison by default with ===, provide a comparison function
         * if you want custom comparison).
         * */
        equals(another: Maybe<T>, comparaisonFunction?: (some: T, another: T) => boolean): boolean;
        /**
         * Performs the specified action (read : side-effect) in this context ; if there is no value, nothing will happen.
         * @param callbackfn  A function that accepts up to three arguments. forEach calls the callbackfn function one time for each element in the array.
         */
        forEach(callbackfn: (value: T) => void): void;
        /** Apply a side effect function into this Maybe. */
        tap(tapFunction: (value: T) => void): Maybe<T>;
    }
    interface MaybeMatcher<A, B> {
        empty: () => B;
        just: (a: A) => B;
    }
    /**
     *
     * Contains either an `A`, or a `B`.
     *
     * An instantiated Either will either be
     * - a Left&lt;A>
     * - or a Right&lt;B>
     * depending on the type of the data you put into.
     *
     * Represents a disjunction : a result that is either an `A` or a `B`.
     *
     * ADT : A sum type.
     */
    interface Either<A, B> {
        /** Return `true` if this disjunction is left. */
        isLeft(): boolean;
        /** Return `true` if this disjunction is right. */
        isRight(): boolean;
        /** Catamorphism. Run the first given function if left, otherwise, the second given function. */
        fold<X>(l: (a: A) => X, r: (n: B) => X): X;
        /** todo : Flip the left/right values in this disjunction. */
        swap(): Either<B, A>;
        /** Run the given function on the left value. */
        leftMap<C>(f: (a: A) => C): Either<C, B>;
        /** Map on the right of this disjunction. */
        map<D>(g: (b: B) => D): Either<A, D>;
        /** FlatMap on the right of this disjunction. */
        flatMap<C>(f: (b: B) => Either<A, C>): Either<A, C>;
        /** Return the right value of this disjunction, otherwise the provided fallback value (as a thunk). */
        getOrElse(x: () => B): B;
        /** Return the right value of this disjunction or run the given function on the left. */
        valueOr(x: (a: A) => B): B;
        /** Return this if it is a right, otherwise the provided fallback value (as a thunk). */
        orElse<AA extends A, BB extends B>(x: () => Either<AA, BB>): Either<AA | A, BB | B>;
        match<C, D>(m: EitherMatcher<A, B, C, D>): C | D;
        /** Apply a side effect function on the right of this Either. */
        tap(tapFunction: (value: B) => void): Either<A, B>;
    }
    interface EitherMatcher<A, B, C, D> {
        left: (a: A) => C;
        right: (b: B) => D;
    }
    /**
     * A 'bag' with two values.
     *
     * Represents a conjunction : a result that is both an `A` and a `B`.
     *
     * ADT : A product type
     * */
    interface Tuple2<A, B> {
        /** Return the first value. */
        first(): A;
        /** Return the second value. */
        second(): B;
        /** Map over both sides. */
        bimap<C, D>(firstF: (a: A) => C, secondF: (b: B) => D): Tuple2<C, D>;
    }
    /** A type that exposes an equality check method to compare two same type instances. */
    interface Eq {
        /** Return if the current instance is equal to another instance. */
        equals(another: Eq): boolean;
    }
    type EqOrPrimitive = Eq | number | string;
    /**
     * A collection with no duplicates permitted.
     * Duplicates detection should be based on object equality.
     * */
    interface Set<A extends EqOrPrimitive> {
        /**
         * Gets the length of the set. This is a number one higher than the highest element defined in the set.
         */
        length: number;
        /** Return a new set with the element added, if the set does not already contains it ;
         *  otherwise, return the original set unchanged and drop the element.
         * */
        add(a: A): Set<A>;
        head(): api.Maybe<A>;
        tail(): Set<A>;
        last(): api.Maybe<A>;
        /**
         * Remove the element from the set if it exists, returning the updated set ;
         * otherwise, return the original set unchanged.
         * @param a the element to remove
         */
        remove(a: A): Set<A>;
        /**
         * Returns a string representation.
         */
        toString(): string;
        toLocaleString(): string;
        /**
         * Return a copy of the underlying js array that back this set.
         */
        toArray(): Array<A>;
        /** Return the union set of two sets.*/
        union(s: Set<A>): Set<A>;
        /**
         * Adds all the elements of an array separated by the specified separator string.
         * @param separator A string used to separate one element of an array from the next in the resulting String. If omitted, the array elements are separated with a comma.
         */
        join(separator?: string): string;
        /**
         * Reverses the elements in the Set.
         */
        reverse(): Set<A>;
        /**
         * Returns a section of a Set.
         * @param start The beginning of the specified portion .
         * @param end The end of the specified portion.
         */
        slice(start?: number, end?: number): Set<A>;
        /**
         * Sorts a Set.
         * @param compareFn The name of the function used to determine the order of the elements. If omitted, the elements are sorted in ascending, ASCII character order.
         */
        sort(compareFn?: (a: A, b: A) => number): Set<A>;
        /**
         * Returns the index of the first occurrence of a value in the set.
         * @param searchElement The value to locate.
         * @param fromIndex The array index at which to begin the search. If fromIndex is omitted, the search starts at index 0.
         */
        indexOf(searchElement: A, fromIndex?: number): number;
        /**
         * Returns the index of the last occurrence of a specified value in a Set.
         * @param searchElement The value to locate in the array.
         * @param fromIndex The array index at which to begin the search. If fromIndex is omitted, the search starts at the last index in the array.
         */
        /**
         * Determines whether all the members of a Set satisfy the specified test.
         * @param callbackfn A function that accepts up to three arguments. The every method calls the callbackfn function for each element in array1 until the callbackfn
         *                   returns false, or until the end of the array.
         * @param thisArg An object to which the this keyword can refer in the callbackfn function. If thisArg is omitted, undefined is used as the this value.
         */
        every(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): boolean;
        /**
         * Determines whether the specified callback function returns true for any element of a Set.
         * @param callbackfn A function that accepts up to three arguments. The some method calls the callbackfn function for each element in array1 until the callbackfn
         *                   returns true, or until the end of the array.
         * @param thisArg An object to which the this keyword can refer in the callbackfn function. If thisArg is omitted, undefined is used as the this value.
         */
        some(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): boolean;
        /**
         * Performs the specified action for each element in a Set.
         * @param callbackfn  A function that accepts up to three arguments. forEach calls the callbackfn function one time for each element in the array.
         * @param thisArg  An object to which the this keyword can refer in the callbackfn function. If thisArg is omitted, undefined is used as the this value.
         */
        forEach(callbackfn: (value: A, index: number, array: A[]) => void, thisArg?: any): void;
        /**
         * Calls a defined callback function on each element of a Set, and returns a Set that contains the results.
         * @param callbackfn A function that accepts up to three arguments. The map method calls the callbackfn function one time for each element in the array.
         * @param thisArg An object to which the this keyword can refer in the callbackfn function. If thisArg is omitted, undefined is used as the this value.
         */
        map<U extends EqOrPrimitive>(callbackfn: (value: A, index: number, array: A[]) => U, thisArg?: any): Set<U>;
        /**
         * Returns the elements of a Set that meet the condition specified in a callback function.
         *
         * @param callbackfn A function that accepts up to three arguments.
         *                    The filter method calls the callbackfn function one time for each element in the array.
         * @param thisArg An object to which the this keyword can refer in the callbackfn function.
         *                    If thisArg is omitted, undefined is used as the this value.
         */
        filter(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): Set<A>;
        /**
         * The inverse of the filter method : returns the elements that does not meet the condition specified in a callback function.
         *
         * @param callbackfn A function that accepts up to three arguments.
         *                    The filter method calls the callbackfn function one time for each element in the array.
         * @param thisArg An object to which the this keyword can refer in the callbackfn function.
         *                    If thisArg is omitted, undefined is used as the this value.
         */
        filterNot(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): Set<A>;
        /**
         *  Finds the first element of the set satisfying a predicate, if any.
         *  Note: might return different results for different runs, unless the underlying collection type is ordered.
         *  @param p   the predicate used to test elements.
         *  @returns a Maybe value containing the first element in the set that satisfies p, or None if none exists.
         */
        find(p: (a: A) => boolean): api.Maybe<A>;
        /**
         * Calls the specified callback function for all the elements in a Set. The return value of the callback function is the accumulated result, and is provided as an argument
         *   in the next call to the callback function.
         * @param callbackfn A function that accepts up to four arguments. The reduce method calls the callbackfn function one time for each element in the array.
         * @param initialValue If initialValue is specified, it is used as the initial value to start the accumulation. The first call to the callbackfn function provides this value
         *                      as an argument instead of a Set value.
         */
        reduce(callbackfn: (previousValue: A, currentValue: A, currentIndex: number, array: A[]) => A, initialValue?: A): A;
        /**
         * Calls the specified callback function for all the elements in a Set. The return value of the callback function is the accumulated result, and is provided as an argument
         *  in the next call to the callback function.
         * @param callbackfn A function that accepts up to four arguments. The reduce method calls the callbackfn function one time for each element in the array.
         * @param initialValue If initialValue is specified, it is used as the initial value to start the accumulation. The first call to the callbackfn function provides this value
         *                     as an argument instead of a Set value.
         */
        reduce<U>(callbackfn: (previousValue: U, currentValue: A, currentIndex: number, array: A[]) => U, initialValue: U): U;
        /**
         * Calls the specified callback function for all the elements in a Set, in descending order. The return value of the callback function is the accumulated result,
         *  and is provided as an argument in the next call to the callback function.
         * @param callbackfn A function that accepts up to four arguments. The reduceRight method calls the callbackfn function one time for each element in the array.
         * @param initialValue If initialValue is specified, it is used as the initial value to start the accumulation. The first call to the callbackfn function provides this value
         *                     as an argument instead of a Set value.
         */
        reduceRight(callbackfn: (previousValue: A, currentValue: A, currentIndex: number, array: A[]) => A, initialValue?: A): A;
        /**
         * Calls the specified callback function for all the elements in a Set, in descending order. The return value of the callback function is the accumulated result,
         *  and is provided as an argument in the next call to the callback function.
         * @param callbackfn A function that accepts up to four arguments. The reduceRight method calls the callbackfn function one time for each element in the array.
         * @param initialValue If initialValue is specified, it is used as the initial value to start the accumulation. The first call to the callbackfn function provides this value
         *                     as an argument instead of a Set value.
         */
        reduceRight<U>(callbackfn: (previousValue: U, currentValue: A, currentIndex: number, array: A[]) => U, initialValue: U): U;
    }
    namespace Predicates {
        function nullOrEmptyStringPredicate(s: string): boolean;
        function nullOrEmptyPredicate(x: any): boolean;
    }
    interface Try<A> extends Either<Error, A> {
    }
}
