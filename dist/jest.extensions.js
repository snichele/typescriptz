"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
expect.extend({
    definedAndContains: function (received, argument) {
        if (received["match"] === null) {
            return ({
                message: function () { return " 'definedAndContains' should be applied to a typescriptz Maybe !"; },
                pass: false
            });
        }
        else {
            return received.match({
                empty: function () { return ({ message: function () { return "Received value was empty !"; }, pass: false }); },
                just: function (x) { return (x === argument) ?
                    ({ message: function () { return "Correctly contains " + x + " compared with strict '==='"; }, pass: true }) :
                    ({ message: function () { return "Expected Maybe content '" + argument + "' is different (!==) of '" + x + "'"; }, pass: false }); }
            });
        }
    }
});
//# sourceMappingURL=jest.extensions.js.map