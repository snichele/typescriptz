"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Maybe_1 = require("./core/Maybe");
String.prototype.just = function () {
    return Maybe_1.Maybe.fromNullable(this);
};
Boolean.prototype.just = function () {
    return Maybe_1.Maybe.fromNullable(this);
};
Number.prototype.just = function () {
    return Maybe_1.Maybe.fromNullable(this);
};
String.fromNullable = function (val) {
    return Maybe_1.Maybe.fromNullable(val);
};
Boolean.fromNullable = function (val) {
    return Maybe_1.Maybe.fromNullable(val);
};
Number.fromNullable = function (val) {
    return Maybe_1.Maybe.fromNullable(val);
};
//# sourceMappingURL=globals.extensions.js.map