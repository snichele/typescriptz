"use strict";
/**
 * Operations on functions.
 * @Deprecated use Fonction1
 **/
Object.defineProperty(exports, "__esModule", { value: true });
var FunctionsOps;
(function (FunctionsOps) {
    /**
     * @Deprecated use Fonction1
     *
     * Build a context for function composition chaining with a nice DSL.
     *
     *   first(f1).then(f2).andThen(f3)...
     *
     * finish with finish(), and get back your final function to execute !
     *
     *   const composedFunction = first(f1).then(f2).andThen(f3).finish();
     *
     * @param f the first function of the composition
     * @returns {typescriptz.core.FunctionsOps.ComposeDSL}
     */
    function first(f) {
        var c = new Composer();
        c.then(f);
        return c;
    }
    FunctionsOps.first = first;
    var Composer = /** @class */ (function () {
        function Composer() {
            this.functions = [];
        }
        Composer.prototype.then = function (f) {
            this.functions.push(f);
            return this;
        };
        Composer.prototype.andThen = function (f) {
            this.functions.push(f);
            return this;
        };
        Composer.prototype.finish = function () {
            return this.functions.reduce(function (memo, next) {
                return memo(next);
            }, doCompose)();
        };
        return Composer;
    }());
    /* tslint:disable:typedef */
    /* Based on http://scott.sauyet.com/Javascript/Talk/Compose/2013-05-22/ */
    function doCompose(f) {
        var queue = f ? [f] : [];
        return function fn(g) {
            if (arguments.length) {
                queue.push(g);
                return fn;
            }
            return function () {
                var args = Array.prototype.slice.call(arguments);
                queue.forEach(function (func) {
                    args = [func.apply(this, args)];
                });
                return args[0];
            };
        };
    }
})(FunctionsOps = exports.FunctionsOps || (exports.FunctionsOps = {}));
//# sourceMappingURL=FunctionsOps.js.map