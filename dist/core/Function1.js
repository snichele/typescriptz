"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Wrap a pure function into 'Function1' to simplify function composition.
 * (Lawful regarding Functor laws.)
 */
var Function1 = /** @class */ (function () {
    function Function1(apply) {
        var _this = this;
        this.apply = apply;
        /**
         * Compose the underlying function with another one.
         *  A -> B andThen B -> C gives A -> C
         */
        this.andThen = function (another) {
            return new Function1(function (a) {
                return another(_this.apply(a));
            });
        };
    }
    return Function1;
}());
exports.Function1 = Function1;
//# sourceMappingURL=Function1.js.map