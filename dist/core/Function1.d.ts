/**
 * Wrap a pure function into 'Function1' to simplify function composition.
 * (Lawful regarding Functor laws.)
 */
export declare class Function1<A, B> {
    readonly apply: (a: A) => B;
    constructor(apply: (a: A) => B);
    /**
     * Compose the underlying function with another one.
     *  A -> B andThen B -> C gives A -> C
     */
    andThen: <C>(another: (b: B) => C) => Function1<A, C>;
}
