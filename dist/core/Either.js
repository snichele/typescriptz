"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * api.Either related operations.
 *
 * @see {@link api.Either}
 */
var Either;
(function (Either) {
    /** Construct a right disjunction value. */
    function Right(x) {
        return newEither(undefined, x);
    }
    Either.Right = Right;
    /** Construct a left disjunction value. */
    function Left(x) {
        return newEither(x, undefined);
    }
    Either.Left = Left;
    var EitherImpl = /** @class */ (function () {
        function EitherImpl(a, b) {
            this.a = a;
            this.b = b;
            this.throwIllegalEitherState = function () {
                throw new Error("Illegal Either state !");
            };
            if (a !== undefined && b !== undefined) {
                throw new Error("An Either cannot be constructed from a left AND a right value !");
            }
        }
        EitherImpl.prototype.isLeft = function () {
            return (this.a !== undefined);
        };
        EitherImpl.prototype.isRight = function () {
            return (this.b !== undefined);
        };
        EitherImpl.prototype.fold = function (l, r) {
            return (this.isLeft()) ? l(this.a) : r(this.b);
        };
        // in the not part of the ternary below, the casting to C,B is done to force the type.
        EitherImpl.prototype.leftMap = function (f) {
            return (this.isLeft()) ? newEither(f(this.a), this.b) : newEither(undefined, this.b);
        };
        EitherImpl.prototype.map = function (g) {
            return (this.isRight()) ? newEither(undefined, g(this.b)) : newEither(this.a, undefined);
        };
        EitherImpl.prototype.flatMap = function (f) {
            return (this.isRight()) ?
                f(this.b) :
                newEither(this.a, undefined);
        };
        EitherImpl.prototype.getOrElse = function (x) {
            if (this.isRight()) {
                return this.b;
            }
            else {
                if (this.isLeft()) {
                    return x();
                }
                else {
                    return this.throwIllegalEitherState();
                }
            }
        };
        EitherImpl.prototype.valueOr = function (x) {
            if (this.isRight()) {
                return this.b;
            }
            else {
                if (this.isLeft()) {
                    return x(this.a);
                }
                else {
                    return this.throwIllegalEitherState();
                }
            }
        };
        EitherImpl.prototype.orElse = function (x) {
            return (this.isRight()) ? this : (this.isLeft()) ? x() : (this.throwIllegalEitherState());
        };
        EitherImpl.prototype.swap = function () {
            return newEither(this.b, this.a);
        };
        EitherImpl.prototype.match = function (m) {
            return (this.isLeft()) ? m.left(this.a) : m.right(this.b);
        };
        EitherImpl.prototype.tap = function (tapFunction) {
            this.match({
                left: function () { return void 0; },
                right: function (b) {
                    return tapFunction(b);
                }
            });
            return this;
        };
        return EitherImpl;
    }());
    /**
     * This builder function accepts 'undefined' values, but is considered 'safe' at runtime =>
     *  there is no method in the API to directly 'unwrap' a value from the Either instance, so
     *  undefined is not supposed to 'escape'.
     *
     * @param {A | undefined} a
     * @param {B | undefined} b
     * @returns {api.Either<A, B>}
     */
    var newEither = function (a, b) {
        return new EitherImpl(a, b);
    };
})(Either = exports.Either || (exports.Either = {}));
//# sourceMappingURL=Either.js.map