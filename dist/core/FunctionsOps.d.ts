/**
 * Operations on functions.
 * @Deprecated use Fonction1
 **/
export declare namespace FunctionsOps {
    interface ComposeDSL {
        then(f: Function): ComposeDSL;
        andThen(f: Function): ComposeDSL;
        finish(): (a: any) => any;
    }
    /**
     * @Deprecated use Fonction1
     *
     * Build a context for function composition chaining with a nice DSL.
     *
     *   first(f1).then(f2).andThen(f3)...
     *
     * finish with finish(), and get back your final function to execute !
     *
     *   const composedFunction = first(f1).then(f2).andThen(f3).finish();
     *
     * @param f the first function of the composition
     * @returns {typescriptz.core.FunctionsOps.ComposeDSL}
     */
    function first(f: (args: any) => any): ComposeDSL;
}
