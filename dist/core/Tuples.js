"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * api.Tuples related operations.
 *
 * @see {@link api.Tuples}
 */
var Tuples;
(function (Tuples) {
    function Tuple2(a, b) {
        return new Tuple2_(a, b);
    }
    Tuples.Tuple2 = Tuple2;
    /* tslint:disable:class-name */
    var Tuple2_ = /** @class */ (function () {
        function Tuple2_(first, second) {
            this._first = first;
            this._second = second;
        }
        Tuple2_.prototype.first = function () { return this._first; };
        Tuple2_.prototype.second = function () { return this._second; };
        Tuple2_.prototype.bimap = function (firstF, secondF) {
            return new Tuple2_(firstF(this._first), secondF(this._second));
        };
        return Tuple2_;
    }());
    Tuples.Tuple2_ = Tuple2_;
    /* tslint:enable:class-name */
})(Tuples = exports.Tuples || (exports.Tuples = {}));
//# sourceMappingURL=Tuples.js.map