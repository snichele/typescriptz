"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ListOps_1 = require("./ListOps");
/**
 * api.Eq related operations.
 *
 * @see {@link api.Eq}
 */
var Eq;
(function (Eq) {
    /**
     * Two WithCode concrete instances are considered equals if :
     * - they have the same constructor
     * - they have the same 'code' property value.
     */
    var WithCodeEq = /** @class */ (function () {
        function WithCodeEq(code) {
            this.code = code;
        }
        WithCodeEq.prototype.equals = function (another) {
            return (another instanceof WithCodeEq) ? another.code === this.code : false;
        };
        return WithCodeEq;
    }());
    Eq.WithCodeEq = WithCodeEq;
    (function (WithCodeEq) {
        WithCodeEq.areEquals = function (x) { return function (y) { return x.equals(y); }; };
        WithCodeEq.isPairEquals = function (pair) { return pair[0].equals(pair[1]); };
        WithCodeEq.getCode = function (x) { return x.code; };
        WithCodeEq.findInArray = function (code) { return function (xs) {
            return ListOps_1.ListOps.maybeHead(xs.filter(function (_) { return _.code === code; }));
        }; };
        WithCodeEq.of = function (s) { return new WithCodeEq(s); };
    })(WithCodeEq = Eq.WithCodeEq || (Eq.WithCodeEq = {}));
})(Eq = exports.Eq || (exports.Eq = {}));
//# sourceMappingURL=Eq.js.map