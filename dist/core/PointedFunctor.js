"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PointedFunctor = /** @class */ (function () {
    function PointedFunctor(functor, pointed) {
        this.functor = functor;
        this.pointed = pointed;
    }
    PointedFunctor.prototype.point = function (a) {
        return this.pointed.point(a);
    };
    PointedFunctor.prototype.fmap = function (f) {
        return this.functor.fmap(f);
    };
    return PointedFunctor;
}());
exports.PointedFunctor = PointedFunctor;
//# sourceMappingURL=PointedFunctor.js.map