"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Maybe_1 = require("./Maybe");
/**
 * js 'Map' operations.
 *
 * Basically, provides common functional programming friendly functions for acting on js Maps.
 */
var MapOps;
(function (MapOps) {
    /**
     * From a Map<A,B>, return a function to
     * return a Just<B> with the getted (key:A) element wrapped,
     * an Empty<B> if there is no such element.
     * (curryfied form)
     *
     * @param from the Map to maybe get something from.
     * @param key the key to use to retrieve element.
     * @returns {Maybe.Maybe<B>} getted value wrapped in a Maybe.
     */
    function maybeGet(from) {
        return function (key) { return Maybe_1.Maybe.fromNullable(from.get(key)); };
    }
    MapOps.maybeGet = maybeGet;
    function maybeGetOr(from, defaultValue) {
        return function (key) {
            return maybeGet(from)(key).getOrElse(defaultValue);
        };
    }
    MapOps.maybeGetOr = maybeGetOr;
})(MapOps = exports.MapOps || (exports.MapOps = {}));
//# sourceMappingURL=MapOps.js.map