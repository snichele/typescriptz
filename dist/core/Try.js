"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Either_1 = require("./Either");
var Try;
(function (Try) {
    Try.from = function (mayThrows) {
        try {
            return Either_1.Either.Right(mayThrows());
        }
        catch (e) {
            return Either_1.Either.Left(e);
        }
    };
})(Try = exports.Try || (exports.Try = {}));
//# sourceMappingURL=Try.js.map