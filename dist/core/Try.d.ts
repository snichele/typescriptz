import { api } from "../api/typescriptz-api";
export declare namespace Try {
    const from: <A>(mayThrows: () => A) => api.Either<Error, A>;
}
