"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ListOps_1 = require("./ListOps");
var Maybe_1 = require("./Maybe");
/**
 * api.Set related operations.
 *
 * @see {@link api.Set}
 */
var Set;
(function (Set) {
    /**
     * Smart constructor for generating a set from a TS array.
     * The original array will be cloned and deduplicated.
     *
     * /!\ It will deduplicate the array content by retaining the first occurence
     * of each duplicated element !
     *
     * */
    function fromArray(array) {
        return new Set_(array);
    }
    Set.fromArray = fromArray;
    var checkEqualityOrEqOrPrimitive = function (a) { return function (_) {
        if (typeof _ === "number" || typeof _ === "string") {
            return _ === a;
        }
        else {
            if (_["equals"]) { // instanceof Eq does not work, compiler error ???
                return _.equals(a);
            }
            else {
                /* tslint:disable:triple-equals */
                return _ == a;
                /* tslint:enable:triple-equals */
            }
        }
    }; };
    var checkNonEqualityOrEqOrPrimitive = function (a) { return function (_) {
        return !checkEqualityOrEqOrPrimitive(a)(_);
    }; };
    var arrayAlreadyContainsItem = function (array, item) {
        return array.filter(checkEqualityOrEqOrPrimitive(item)).length !== 0;
    };
    /* tslint:disable:class-name */
    var Set_ = /** @class */ (function () {
        function Set_(arrayToDeduplicateFirst) {
            // check for duplicates !
            this._underlyingArray = arrayToDeduplicateFirst.reduce(function (memo, next) {
                return (arrayAlreadyContainsItem(memo, next)) ? memo : memo.concat([next]);
            }, []);
        }
        Object.defineProperty(Set_.prototype, "length", {
            get: function () {
                return this._underlyingArray.length;
            },
            enumerable: true,
            configurable: true
        });
        Set_.prototype.toArray = function () {
            return this._underlyingArray.slice(0); // defensive copying !
        };
        Set_.prototype.add = function (a) {
            if (arrayAlreadyContainsItem(this._underlyingArray, a)) {
                return this;
            }
            else {
                return new Set_(this.toArray().concat([a]));
            }
        };
        Set_.prototype.remove = function (a) {
            return new Set_(this.toArray().filter(checkNonEqualityOrEqOrPrimitive(a)));
        };
        Set_.prototype.union = function (s) {
            return s.toArray().reduce(function (_1, _2) {
                return _1.add(_2);
            }, this);
        };
        Set_.prototype.join = function (separator) {
            return this._underlyingArray.join(separator);
        };
        Set_.prototype.reverse = function () {
            return new Set_(this._underlyingArray.slice(0).reverse()); // duplicate to avoid side effects !
        };
        Set_.prototype.slice = function (start, end) {
            return new Set_(this._underlyingArray.slice(start, end));
        };
        Set_.prototype.sort = function (compareFn) {
            return new Set_(this._underlyingArray.slice(0).sort(compareFn));
        };
        Set_.prototype.indexOf = function (searchElement, fromIndex) {
            if (typeof searchElement === "number" || typeof searchElement === "string") {
                return this._underlyingArray.indexOf(searchElement, fromIndex);
            }
            else {
                if (searchElement["equals"]) { // instanceof Eq does not work, compiler error ???
                    var initialValue = [0, -1];
                    return (this._underlyingArray.reduce(function (memo, elt) {
                        if (elt.equals(searchElement)) { // I'm hacking all the way... :(
                            return [memo[0] + 1, memo[0]]; // store the index found
                        }
                        return [memo[0] + 1, memo[1]];
                    }, initialValue))[1];
                }
                else {
                    return this._underlyingArray.indexOf(searchElement, fromIndex);
                }
            }
        };
        Set_.prototype.every = function (callbackfn, thisArg) {
            return this._underlyingArray.every(callbackfn);
        };
        Set_.prototype.some = function (callbackfn, thisArg) {
            return this._underlyingArray.some(callbackfn);
        };
        Set_.prototype.forEach = function (callbackfn, thisArg) {
            this._underlyingArray.forEach(callbackfn);
        };
        Set_.prototype.find = function (p) {
            return ListOps_1.ListOps.maybeHead(this._underlyingArray.filter(p));
        };
        Set_.prototype.map = function (callbackfn, thisArg) {
            return new Set_(this._underlyingArray.map(callbackfn));
        };
        Set_.prototype.filter = function (callbackfn, thisArg) {
            return new Set_(this._underlyingArray.filter(callbackfn));
        };
        Set_.prototype.filterNot = function (callbackfn, thisArg) {
            return new Set_(this._underlyingArray.filter(function (value, index, array) {
                return !callbackfn(value, index, array);
            }));
        };
        Set_.prototype.reduce = function (callbackfn, initialValue) {
            return this._underlyingArray.reduce(callbackfn, initialValue);
        };
        Set_.prototype.reduceRight = function (callbackfn, initialValue) {
            return this._underlyingArray.reduceRight(callbackfn, initialValue);
        };
        Set_.prototype.head = function () {
            return Maybe_1.Maybe.fromNullable(this._underlyingArray[0]);
        };
        Set_.prototype.tail = function () {
            return Set.fromArray(this._underlyingArray.slice(1));
        };
        Set_.prototype.last = function () {
            return Maybe_1.Maybe.fromNullable(this._underlyingArray[this._underlyingArray.length - 1]);
        };
        return Set_;
    }());
    Set.Set_ = Set_;
    /* tslint:enable:class-name */
})(Set = exports.Set || (exports.Set = {}));
//# sourceMappingURL=Set.js.map