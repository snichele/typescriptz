/**
 * Combinators is a namespace that provides several combinators functions.
 *
 * To put it in a nutshell : several utility function to combine other functions in ways that comes again and again (it's
 * like design patterns for function combination.)
 *
 * (See https://en.wikipedia.org/wiki/Combinatory_logic for the theory behind the vocabulary).
 *
 */
export declare namespace Combinators {
    /**
     * Kestrel combinator ; takes a A, apply f to A, and return A
     *
     * Useful for creating an object, executing some code on it and return immediately with it.
     *
     * See [Coding patterns] section in tutorials for examples.
     *
     * @param a the object to return
     * @param f the side effect only function to apply to the object
     * @returns {A}
     */
    function K<A>(a: A, f: (theA: A) => void): A;
    /** Alias for the K combinator. */
    function using<A>(a: A, f: (theA: A) => void): A;
}
