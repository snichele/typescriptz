import { api } from "../api/typescriptz-api";
/**
 * api.Tuples related operations.
 *
 * @see {@link api.Tuples}
 */
export declare namespace Tuples {
    function Tuple2<A, B>(a: A, b: B): api.Tuple2<A, B>;
    class Tuple2_<A, B> implements api.Tuple2<A, B> {
        private _first;
        private _second;
        constructor(first: A, second: B);
        first(): A;
        second(): B;
        bimap<C, D>(firstF: (a: A) => C, secondF: (b: B) => D): api.Tuple2<C, D>;
    }
}
