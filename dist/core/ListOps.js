"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Maybe_1 = require("./Maybe");
var Tuples_1 = require("./Tuples");
/**
 * Lists operations.
 *
 * Basically, provides common functional programming lists functions for acting on js arrays.
 */
var ListOps;
(function (ListOps) {
    /**
     * Return a Just with the first array element wrapped, an Empty if there is no such element.
     *
     * @param as the array to maybe extract head from.
     * @returns {Maybe.Maybe<A>} head wrapped in a Maybe.
     */
    function maybeHead(as) {
        var headArray = (as == null) ? [] : as.slice(0, 1);
        return (headArray.length === 0) ? Maybe_1.Maybe.Empty() : Maybe_1.Maybe.Just(headArray[0]);
    }
    ListOps.maybeHead = maybeHead;
    /**
     * Returns an array formed from the first array and a second array by combining corresponding elements in pairs.
     * If one of the two collections is longer than the other, its remaining elements are ignored.
     *
     * @param firstArray
     * @param secondArray
     */
    function zip(firstArray, secondArray) {
        return (firstArray.map(function (v, idx) {
            return (secondArray[idx]) ? Tuples_1.Tuples.Tuple2(v, secondArray[idx]) : void 0;
        }).filter(function (tuple) { return tuple !== void 0; }));
    }
    ListOps.zip = zip;
    /**
     * Execute the lambda (toExecuteIfEmpty, toExecuteIfOne, toExecuteIfMoreThanOne)
     * corresponding to the length of the array.
     *
     * @param array
     * @param toExecuteIfEmpty
     * @param toExecuteIfOne
     * @param toExecuteIfMoreThanOne
     * @return {B}
     */
    function lengthMatcher(array, toExecuteIfEmpty, toExecuteIfOne, toExecuteIfMoreThanOne) {
        return (array.length === 0) ? toExecuteIfEmpty() :
            (array.length === 1) ? toExecuteIfOne(array) : toExecuteIfMoreThanOne(array);
    }
    ListOps.lengthMatcher = lengthMatcher;
    /**
     * Will group the following array of A, using the 'extractorFunction' to get
     * the key value from each A to use for grouping.
     * Will return an array of Tuple2<B, Array<A>>.
     * Example :
     *
     * class Toto {
     *    static xValue = (t: Toto) => t.x;
     *    constructor(public x: string, public y: number) { }
     * }
     *
     *   const ts = [new Toto("a", 10), new Toto("a", 12), new Toto("b", 15)];
     *   const result: Tuple2<string, Array<Toto>> = ListOps.groupBy(ts, Toto.xValue);
     *
     * @param array
     * @param extractorFunction
     * @returns {A|Array<Tuple2<any, Array<any>>>}
     */
    function groupBy(array, extractorFunction) {
        return array.reduce(function (memo, a) {
            return ListOps.maybeHead(memo.filter(function (_) {
                return _.first() === extractorFunction(a);
            })).match({
                empty: function () {
                    return memo.concat([Tuples_1.Tuples.Tuple2(extractorFunction(a), [a])]);
                },
                just: function (existingTuple) {
                    return memo.filter(function (_) { return _ !== existingTuple; }).concat([
                        Tuples_1.Tuples.Tuple2(extractorFunction(a), existingTuple.second().concat([a]))
                    ]);
                }
            });
        }, []);
    }
    ListOps.groupBy = groupBy;
    /**
     * Maybe return the only element of an Array, IIF this array, after filtering
     * with the predicate 'f', has only one element.
     * Otherwise, return Maybe.Empty for all other cases (array empty, array with more than one element...)
     */
    function onlyOneThatMatch(ts, match) {
        return Maybe_1.Maybe.Just(ts.filter(function (_) { return match(_); }) // Take all that match
        ).filter(function (_) {
            return _.length === 1;
        } // Only one should match, otherwhise, it's KO !
        ).map(function (_) {
            return _[0];
        } // Get the only one item.
        );
    }
    ListOps.onlyOneThatMatch = onlyOneThatMatch;
})(ListOps = exports.ListOps || (exports.ListOps = {}));
//# sourceMappingURL=ListOps.js.map