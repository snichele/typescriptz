import { api } from "../api/typescriptz-api";
import { PointedFunctor } from "./PointedFunctor";
export declare class Applicative<A> implements api.Applicative<A> {
    private pointedFunctor;
    private _applic;
    constructor(pointedFunctor: PointedFunctor<A>, applic: api.Applic);
    functor<B>(): {
        fmap: (f: (a: A) => B) => (c: api.Context<A>) => api.Context<B>;
    };
    pointed(): {
        point: (f: () => A) => api.Context<A>;
    };
    fmap<B>(f: (a: A) => B): (ma: any) => any;
    point(a: () => A): api.Context<A>;
    applic<B>(f: api.Context<(a: A) => B>): (p1: api.Context<A>) => api.Context<B>;
    ap<B>(f: api.Context<(a: A) => B>): (ctx: api.Context<A>) => api.Context<B>;
}
