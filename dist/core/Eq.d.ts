import { api } from "../api/typescriptz-api";
/**
 * api.Eq related operations.
 *
 * @see {@link api.Eq}
 */
export declare namespace Eq {
    /**
     * Two WithCode concrete instances are considered equals if :
     * - they have the same constructor
     * - they have the same 'code' property value.
     */
    class WithCodeEq implements api.Eq {
        code: string;
        constructor(code: string);
        equals(another: api.Eq): boolean;
    }
    namespace WithCodeEq {
        const areEquals: (x: WithCodeEq) => (y: WithCodeEq) => boolean;
        const isPairEquals: (pair: [WithCodeEq, WithCodeEq]) => boolean;
        const getCode: (x: WithCodeEq) => string;
        const findInArray: (code: string) => <T extends WithCodeEq>(xs: T[]) => api.Maybe<T>;
        const of: (s: string) => WithCodeEq;
    }
}
