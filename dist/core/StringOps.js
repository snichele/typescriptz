"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Strings related functions.
 *
 * Because StringUtils is so 1995 !
 */
var StringOps;
(function (StringOps) {
    StringOps.longerThan = function (l) {
        return function (s) {
            return s.length > l;
        };
    };
    StringOps.isEmpty = function (str) {
        return str === null || str === undefined ? true : /^[\s\xa0]*$/.test(str);
    };
    StringOps.isNotEmptyOrEmptyString = function (str) {
        return !StringOps.isEmpty(str);
    };
    StringOps.between = function (start, end) {
        return function (s) {
            return s.substr(start, end);
        };
    };
    StringOps.textBeforeToken = function (token) {
        return function (str) {
            return StringOps.between(0, str.indexOf(token))(str);
        };
    };
    StringOps.before = function (position) {
        return StringOps.between(0, position);
    };
    StringOps.after = function (position) {
        return StringOps.between(position, void 0);
    };
    StringOps.capitalize = function (str) {
        return str.substr(0, 1).toUpperCase() + str.substring(1).toLowerCase();
    };
    StringOps.bindTemplate = function (template, values) {
        return template.replace(/\${(.*?)}/g, function (match, p1) { return values[p1]; });
    };
})(StringOps = exports.StringOps || (exports.StringOps = {}));
//# sourceMappingURL=StringOps.js.map