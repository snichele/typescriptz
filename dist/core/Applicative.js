"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Applicative = /** @class */ (function () {
    function Applicative(pointedFunctor, applic) {
        this.pointedFunctor = pointedFunctor;
        this._applic = applic;
    }
    Applicative.prototype.functor = function () {
        var _this = this;
        return {
            fmap: function (f) {
                return _this.pointedFunctor.fmap(f);
            }
        };
    };
    Applicative.prototype.pointed = function () {
        var _this = this;
        return {
            point: function (f) {
                return _this.pointedFunctor.point(f);
            }
        };
    };
    // // any everwhere because typesysctem compilation errors are driving me mad
    Applicative.prototype.fmap = function (f) { return this.functor().fmap(f); };
    Applicative.prototype.point = function (a) { return this.pointed().point(a); };
    Applicative.prototype.applic = function (f) { return this._applic.applic(f); };
    Applicative.prototype.ap = function (f) { return this._applic.applic(f); };
    return Applicative;
}());
exports.Applicative = Applicative;
// export namespace Applicative {
//   export const ofMaybe = <T>() => Maybe.Empty<T>().asApplicative();
// }
//
//# sourceMappingURL=Applicative.js.map