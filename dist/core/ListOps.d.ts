import { api } from "../api/typescriptz-api";
/**
 * Lists operations.
 *
 * Basically, provides common functional programming lists functions for acting on js arrays.
 */
export declare namespace ListOps {
    import Tuple2 = api.Tuple2;
    /**
     * Return a Just with the first array element wrapped, an Empty if there is no such element.
     *
     * @param as the array to maybe extract head from.
     * @returns {Maybe.Maybe<A>} head wrapped in a Maybe.
     */
    function maybeHead<A>(as: Array<A>): api.Maybe<A>;
    /**
     * Returns an array formed from the first array and a second array by combining corresponding elements in pairs.
     * If one of the two collections is longer than the other, its remaining elements are ignored.
     *
     * @param firstArray
     * @param secondArray
     */
    function zip<A, B>(firstArray: Array<A>, secondArray: Array<B>): Array<api.Tuple2<A, B>>;
    /**
     * Execute the lambda (toExecuteIfEmpty, toExecuteIfOne, toExecuteIfMoreThanOne)
     * corresponding to the length of the array.
     *
     * @param array
     * @param toExecuteIfEmpty
     * @param toExecuteIfOne
     * @param toExecuteIfMoreThanOne
     * @return {B}
     */
    function lengthMatcher<A, B>(array: Array<A>, toExecuteIfEmpty: () => B, toExecuteIfOne: (as: Array<A>) => B, toExecuteIfMoreThanOne: (ass: Array<A>) => B): B;
    /**
     * Will group the following array of A, using the 'extractorFunction' to get
     * the key value from each A to use for grouping.
     * Will return an array of Tuple2<B, Array<A>>.
     * Example :
     *
     * class Toto {
     *    static xValue = (t: Toto) => t.x;
     *    constructor(public x: string, public y: number) { }
     * }
     *
     *   const ts = [new Toto("a", 10), new Toto("a", 12), new Toto("b", 15)];
     *   const result: Tuple2<string, Array<Toto>> = ListOps.groupBy(ts, Toto.xValue);
     *
     * @param array
     * @param extractorFunction
     * @returns {A|Array<Tuple2<any, Array<any>>>}
     */
    function groupBy<A, B>(array: Array<A>, extractorFunction: (a: A) => B): Array<Tuple2<B, Array<A>>>;
    /**
     * Maybe return the only element of an Array, IIF this array, after filtering
     * with the predicate 'f', has only one element.
     * Otherwise, return Maybe.Empty for all other cases (array empty, array with more than one element...)
     */
    function onlyOneThatMatch<T>(ts: Array<T>, match: (t: T) => boolean): api.Maybe<T>;
}
