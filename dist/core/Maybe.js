"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var typescriptz_api_1 = require("../api/typescriptz-api");
var Applicative_1 = require("./Applicative");
var FunctionsOps_1 = require("./FunctionsOps");
var PointedFunctor_1 = require("./PointedFunctor");
/**
 * api.Maybe related operations.
 *
 * @see {@link api.Maybe}
 */
var Maybe;
(function (Maybe_1) {
    // --- EXPORTS
    /** Wrap a value in Just, or return Empty if the value is null / undefined */
    function fromNullable(a) {
        return emptyIf(typescriptz_api_1.api.Predicates.nullOrEmptyPredicate, a);
    }
    Maybe_1.fromNullable = fromNullable;
    /** Return a Just containing the first array element if it exists,
     * or return Empty if the array is empty. */
    function headOption(as) {
        return (as[0]) ? Just(as[0]) : Empty();
    }
    Maybe_1.headOption = headOption;
    function emptyIf(predicate, a) {
        return (predicate(a)) ? Empty() : Just(a); // typecast to A because we are 'safe' here !
    }
    Maybe_1.emptyIf = emptyIf;
    /** Return the unique Empty instance. */
    function Empty() {
        return emptyInstance;
    }
    Maybe_1.Empty = Empty;
    /** Wrap the provided value in a Just. */
    function Just(a) {
        return new Just_(a);
    }
    Maybe_1.Just = Just;
    // --- PRIVATE
    /**
     * Private implementation of Maybe.
     **/
    /* tslint:disable:no-shadowed-variable */
    var Maybe = /** @class */ (function () {
        function Maybe() {
        }
        /** Catamorphism.
         * Run the given function on the underlying value if present, otherwise return
         * the provided fallback value */
        Maybe.prototype.cata = function (f, b) {
            if (this instanceof Empty_) {
                return b();
            }
            var just = this; // shitty hack to workaround compiler crazyness
            return f(just.a);
        };
        /** @inheritDoc */
        Maybe.prototype.fold = function (f, b) {
            return this.cata(f, b);
        };
        /** @inheritDoc */
        Maybe.prototype.exists = function (predicate) {
            return this.cata(predicate, function () { return false; });
        };
        Maybe.prototype.filter = function (predicate) {
            var _this = this;
            return this.cata(function (a) { return (predicate(a)) ? _this : Empty(); }, 
            /* tslint:disable:no-unnecessary-callback-wrapper */
            function () { return Empty(); });
        };
        Maybe.prototype.filterNot = function (predicate) {
            var _this = this;
            return this.cata(function (a) { return (!predicate(a)) ? _this : Empty(); }, function () { return Empty(); });
        };
        /** @inheritDoc */
        Maybe.prototype.getOrElse = function (thunk) {
            return this.cata(function (a) { return a; }, thunk);
        };
        /** @inheritDoc */
        Maybe.prototype.orElse = function (m) {
            return this.cata(function (a) { return new Just_(a); }, function () { return m; });
        };
        /** @inheritDoc */
        Maybe.prototype.get = function () {
            return this.cata(function (a) { return a; }, function () { return void 0; });
        };
        /** @inheritDoc */
        Maybe.prototype.isJust = function () {
            return this.cata(function () { return true; }, function () { return false; });
        };
        /** @inheritDoc */
        Maybe.prototype.isDefined = function () {
            return this.cata(function () { return true; }, function () { return false; });
        };
        /** @inheritDoc */
        Maybe.prototype.isEmpty = function () {
            return this.cata(function () { return false; }, function () { return true; });
        };
        /** @inheritDoc */
        Maybe.prototype.map = function (f) {
            /*
             in scalaz
             cata(f andThen just[B], Empty[B] )
             */
            return this.cata(FunctionsOps_1.FunctionsOps.first(f).then(Just).finish(), Empty);
        };
        Maybe.prototype.flatMap = function (f) {
            return this.cata(f, Empty);
        };
        /*
         final def map[B](f: A => B): Maybe[B] =
         cata(f andThen just[B], empty[B])
    
         final def flatMap[B](f: A => Maybe[B]) =
         cata(f, empty[B])
         */
        Maybe.prototype.asApplicative = function () {
            return new MaybeApplicative(); // another shitty hack to workaround compiler crazyness
        };
        Maybe.prototype.match = function (m) {
            return (this.isEmpty()) ?
                m.empty() :
                m.just(this.a); // another shitty hack to workaround compiler crazyness
        };
        Maybe.prototype.equals = function (another, comparaisonFunction) {
            if (comparaisonFunction === void 0) { comparaisonFunction = function (some, another) { return some === another; }; }
            return this.match({
                empty: function () { return another.isEmpty(); },
                just: function (a) {
                    return another.match({
                        empty: function () { return false; },
                        just: function (aa) {
                            return comparaisonFunction(aa, a);
                        }
                    });
                },
            });
        };
        Maybe.prototype.forEach = function (callbackfn) {
            this.match({
                empty: function () { return void 0; },
                just: function (aa) {
                    return callbackfn(aa);
                }
            });
        };
        Maybe.prototype.tap = function (tapFunction) {
            this.match({
                empty: function () { return void 0; },
                just: function (aa) {
                    return tapFunction(aa);
                }
            });
            return this;
        };
        return Maybe;
    }());
    /* tslint:disable:class-name */
    /** Internal (not exported) instance of Maybe.Empty. */
    var Empty_ = /** @class */ (function (_super) {
        __extends(Empty_, _super);
        function Empty_() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Empty_;
    }(Maybe));
    var emptyInstance = new Empty_();
    /** Internal (not exported) instance of Maybe.Just. */
    var Just_ = /** @class */ (function (_super) {
        __extends(Just_, _super);
        function Just_(a) {
            var _this = _super.call(this) || this;
            _this.a = a;
            return _this;
        }
        return Just_;
    }(Maybe));
    // Typeclasses
    var MaybePointed = /** @class */ (function () {
        function MaybePointed() {
        }
        MaybePointed.prototype.point = function (a) {
            return Just(a());
        };
        return MaybePointed;
    }());
    Maybe_1.MaybePointed = MaybePointed;
    var MaybeFunctor = /** @class */ (function () {
        function MaybeFunctor() {
        }
        // any everwhere because typesysctem compilation errors are driving me mad
        MaybeFunctor.prototype.fmap = function (f) {
            return function (ama) { return ama.map(f); };
        };
        return MaybeFunctor;
    }());
    Maybe_1.MaybeFunctor = MaybeFunctor;
    var MaybePointedFunctor = /** @class */ (function (_super) {
        __extends(MaybePointedFunctor, _super);
        function MaybePointedFunctor(functor, pointed) {
            return _super.call(this, functor, pointed) || this;
        }
        return MaybePointedFunctor;
    }(PointedFunctor_1.PointedFunctor));
    Maybe_1.MaybePointedFunctor = MaybePointedFunctor;
    var MaybeApplic = /** @class */ (function () {
        function MaybeApplic() {
        }
        // any everwhere because typesysctem compilation errors are driving me mad
        MaybeApplic.prototype.applic = function (mf) {
            return function (ama) {
                return ama.map(mf.getOrElse(function () { return function (x) { return x; }; }));
            };
        };
        return MaybeApplic;
    }());
    Maybe_1.MaybeApplic = MaybeApplic;
    var MaybeApplicative = /** @class */ (function (_super) {
        __extends(MaybeApplicative, _super);
        function MaybeApplicative() {
            return _super.call(this, new MaybePointedFunctor(new MaybeFunctor(), new MaybePointed()), new MaybeApplic()) || this;
        }
        return MaybeApplicative;
    }(Applicative_1.Applicative));
    Maybe_1.MaybeApplicative = MaybeApplicative;
})(Maybe = exports.Maybe || (exports.Maybe = {}));
//# sourceMappingURL=Maybe.js.map