import { api } from "../api/typescriptz-api";
export declare class PointedFunctor<A> {
    private functor;
    private pointed;
    constructor(functor: api.Functor<A>, pointed: api.Pointed<A>);
    point(a: () => A): api.Context<A>;
    fmap<B>(f: (a: A) => B): (c: api.Context<A>) => api.Context<B>;
}
