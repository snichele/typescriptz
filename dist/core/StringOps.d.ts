/**
 * Strings related functions.
 *
 * Because StringUtils is so 1995 !
 */
export declare namespace StringOps {
    const longerThan: (l: number) => (s: string) => boolean;
    const isEmpty: (str: string) => boolean;
    const isNotEmptyOrEmptyString: (str: string) => boolean;
    const between: (start: number, end?: number | undefined) => (s: string) => string;
    const textBeforeToken: (token: string) => (str: string) => string;
    const before: (position: number) => (s: string) => string;
    const after: (position: number) => (s: string) => string;
    const capitalize: (str: string) => string;
    const bindTemplate: (template: string, values: Object) => string;
}
