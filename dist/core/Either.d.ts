import { api } from "../api/typescriptz-api";
/**
 * api.Either related operations.
 *
 * @see {@link api.Either}
 */
export declare namespace Either {
    /** Construct a right disjunction value. */
    function Right<A, B>(x: B): api.Either<A, B>;
    /** Construct a left disjunction value. */
    function Left<A, B>(x: A): api.Either<A, B>;
}
