import { api } from "../api/typescriptz-api";
/**
 * api.Set related operations.
 *
 * @see {@link api.Set}
 */
export declare namespace Set {
    import EqOrPrimitive = api.EqOrPrimitive;
    /**
     * Smart constructor for generating a set from a TS array.
     * The original array will be cloned and deduplicated.
     *
     * /!\ It will deduplicate the array content by retaining the first occurence
     * of each duplicated element !
     *
     * */
    function fromArray<A extends EqOrPrimitive>(array: Array<A>): api.Set<A>;
    class Set_<A extends EqOrPrimitive> implements api.Set<A> {
        private _underlyingArray;
        constructor(arrayToDeduplicateFirst: Array<A>);
        readonly length: number;
        toArray(): Array<A>;
        add(a: A): api.Set<A>;
        remove(a: A): api.Set<A>;
        union(s: api.Set<A>): api.Set<A>;
        join(separator?: string): string;
        reverse(): api.Set<A>;
        slice(start?: number, end?: number): api.Set<A>;
        sort(compareFn?: (a: A, b: A) => number): api.Set<A>;
        indexOf(searchElement: A, fromIndex?: number): number;
        every(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): boolean;
        some(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): boolean;
        forEach(callbackfn: (value: A, index: number, array: A[]) => void, thisArg?: any): void;
        find(p: (a: A) => boolean): api.Maybe<A>;
        map<U extends EqOrPrimitive>(callbackfn: (value: A, index: number, array: A[]) => U, thisArg?: any): api.Set<U>;
        filter(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): api.Set<A>;
        filterNot(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): api.Set<A>;
        reduce<U>(callbackfn: (previousValue: U, currentValue: A, currentIndex: number, array: A[]) => U, initialValue: U): U;
        reduceRight<U>(callbackfn: (previousValue: U, currentValue: A, currentIndex: number, array: A[]) => U, initialValue: U): U;
        head(): api.Maybe<A>;
        tail(): api.Set<A>;
        last(): api.Maybe<A>;
    }
}
