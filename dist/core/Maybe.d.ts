import { api } from "../api/typescriptz-api";
import { Applicative } from "./Applicative";
import { PointedFunctor } from "./PointedFunctor";
/**
 * api.Maybe related operations.
 *
 * @see {@link api.Maybe}
 */
export declare namespace Maybe {
    /** Wrap a value in Just, or return Empty if the value is null / undefined */
    function fromNullable<A>(a: A | undefined | null): api.Maybe<A>;
    /** Return a Just containing the first array element if it exists,
     * or return Empty if the array is empty. */
    function headOption<A>(as: Array<A>): api.Maybe<A>;
    function emptyIf<A>(predicate: (someA: A | undefined | null) => boolean, a: A | undefined | null): api.Maybe<A>;
    /** Return the unique Empty instance. */
    function Empty<A>(): api.Maybe<A>;
    /** Wrap the provided value in a Just. */
    function Just<A>(a: A): api.Maybe<A>;
    class MaybePointed<A> implements api.Pointed<A> {
        point(a: () => A): api.Maybe<A>;
    }
    class MaybeFunctor<A> implements api.Functor<A> {
        fmap<B>(f: (p1: A) => B): (ma: any) => any;
    }
    class MaybePointedFunctor<A> extends PointedFunctor<A> {
        constructor(functor: api.Functor<A>, pointed: api.Pointed<A>);
    }
    class MaybeApplic implements api.Applic {
        applic<A, B>(mf: api.Maybe<(a: A) => B>): (ma: any) => any;
    }
    class MaybeApplicative<T> extends Applicative<T> {
        constructor();
    }
}
