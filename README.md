# Angular-cli help

Angular-cli is only installed to deal with node_modules versions. 
This node_modules are required :

        > @angular-devkit/build-angular
        > @angular/cli
        > @angular/compiler-cli
        > @angular/compiler
        > @angular/language-service

Use :

        > ng update --all

# TypeScriptZ

The missing link between a <small>somewhat</small> great typesystem and Functional Programming 
efficiency in TypeScript (TS).

Inspired by Scala and Scalaz, this library provides several ADT, Monads and interfaces to permit
a more functional style in TS.

Start discovering the library by following the link <a href="./modules/_api_typescriptz_api_.html">"api/typescriptz-api"</a> on the right menu.

# Technical informations

## Building the library from sources

To build the sources (compiler config on src/main/typescript/tsconfig.json) :

        > npm install
        > npm run test
        > npm run package

Files (compiled sources and ts headers) will be produced in the /dist directory.

## Publishing the library to the registry

To publish the package in the registry :

TODO

## About sources

src/main/typescript/typescriptz : the library .ts files.
src/test/ : jasmine spec files for testing the library.
src/examples/ : some more examples (beyond the specs) of the library usages.

## Testing

Tests are written with jasmine api.
Run tests with `npm test`.
Running tests will produce test related artifacts in /dist/tests, including /code-coverage.

## Code coverage

Code coverage is performed through Karma / Instanbul instrumentation.
The result of code coverage after running the tests is in /dist/tests/code-coverage, as an html
reporting site. This only shows the coverage of covered files. Uncovered files will not appear !

You must upload to sonarqube to see the 0% coverage file.
Before uploading, you must remap coverage with TS sources :
`node ./build-configs/remap-istanbul.js`
(waring : the script is full of project specific hard coded values.)
Then perform an `npm run sonar` to do it (WARING : only works on local sonar for now !)

## TODOS / MEMOS

### git

remove tag

git tag -d 12345
git push origin :refs/tags/12345


