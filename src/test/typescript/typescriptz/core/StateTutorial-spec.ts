import {api} from "../../../../main/typescript/typescriptz/api/typescriptz-api";
import {Maybe} from "../../../../main/typescript/typescriptz/core/Maybe";
import {ListOps} from "../../../../main/typescript/typescriptz/core/ListOps";


// TODO : MAKE SOME GRAPHICAL ILLUSTRATION

const SHOULD_NOT_HAPPEN = () => -1;

describe("'State' Monad", () => {

  describe("is an elegant way to solve *some* immutability induced problems...", () => {

    describe("suppose you have something like an Array", () => {

      describe("and you want to EXTRACT or PUT an INFORMATION that will MUTATE the Array state", () => {

        describe("mutable, the old-fashioned way", () => {

          const numbers: Array<number> = [];

          it("and you want to push, or pop, things off/on it - alas, it's mutability !", () => {
            // write on it
            numbers.push(5);
            numbers.push(6);

            // read from the last state
            let popped = numbers.pop();

            // and check
            expect(popped).toBe(6);
            expect(numbers).toEqual([5]);
          });

        });

        describe("immutable, the safest way", () => {

          it("how do you push and pop ? - tedious AF ...", () => {

            // Datas to work with
            const numbers: ReadonlyArray<number> = [];

            // write on it
            const numbers2 = numbers.concat([5]);
            const numbers3 = numbers2.concat([6]);

            // read from the last state
            const popped = numbers3.slice(numbers3.length - 1)[0];
            const numbersAfterPopped =
              numbers3.slice(0, numbers3.length - 1);

            // and check
            expect(popped).toBe(6);
            expect(numbersAfterPopped).toEqual([5]);
          });

          it("... before continuying, let's materialize some types in this example...", () => {

            // Let's introduce some types to add some semantics
            // and simplify signatures a little bit
            type NumberStack = ReadonlyArray<number>;
            type NumberAndNumberStack = [number, NumberStack];

            const numbers: ReadonlyArray<number> = [5, 6];

            const [num, stack]: NumberAndNumberStack = [
              numbers.slice(numbers.length - 1)[0],
              numbers.slice(0, numbers.length - 1)
            ];

            // and check
            expect(num).toBe(6);
            expect(stack).toEqual([5]);

          });

          it(`... and before continuying, FOCUS deeply on the fact that the case we are studying here
            implies an immutable type T  and another 'derived' type, U, that is constructed
            from the T mutation. IT THE MAIN SELLING POINT HERE : if you only need the T type
            (mutation only act upon it) then you can leave now ! :D`, () => {
          });


          describe("so let's try to improve this...", () => {

            describe("first, let's define functions to perform the operations", () => {

              // Remember types defined earlier
              type NumberStack = ReadonlyArray<number>;
              type NumberAndNumberStack = [number, NumberStack];

              // -- Let's add two more !
              // ...because we forgot a case ealier, we introduce Maybe for the undefined case.
              type MaybeNumberAndNumberStack = [api.Maybe<number>, NumberStack];

              // And this one, for our functions.
              type StackAlterationF = (stack: NumberStack) => MaybeNumberAndNumberStack;

              // Rewriting pop hilited the fact that we forgotten a case
              // in the previous 'quick and dirty' solution... what if the stack is empty ?
              const pop = (stack: NumberStack): MaybeNumberAndNumberStack =>
                (stack.length > 0)
                  ? [
                    ListOps.maybeHead(stack.slice(stack.length - 1)),
                    stack.slice(0, stack.length - 1)
                  ]
                  : [
                    Maybe.Empty(),
                    []
                  ];

              const push = (stack: NumberStack, n: number): NumberStack => stack.concat([n]);

              describe("and run all alterations ", () => {

                it("neater, but not perfect yet...", () => {
                  const numbers: NumberStack = [];

                  // write on it
                  const numbers2 = push(numbers, 5);
                  const numbers3 = push(numbers2, 6);

                  // read from the last state
                  const [popped, numbersAfterPopped]: MaybeNumberAndNumberStack = pop(numbers3);

                  // and check
                  expect(popped.getOrElse(SHOULD_NOT_HAPPEN)).toBe(6);
                  expect(numbersAfterPopped).toEqual([5]);
                });

                describe(`as we can see, we are 'chaining' the 'Stack' object all along...
                        How can we solve this intermediary intensive var problems ?`, () => {

                  it(`let's try the 'pyramid of doom' way to eliminate the vars...
                   as discused before, that's not scalable because extending the logic means
                   modifying the pyramid (violate the OC principle).`, () => {
                    const [poppedAgain, numbersAfterPoppedAgain]: MaybeNumberAndNumberStack =
                      pop(
                        push(
                          push([], 5),
                          6
                        )
                      );
                    expect(poppedAgain.getOrElse(SHOULD_NOT_HAPPEN)).toBe(6);
                    expect(numbersAfterPoppedAgain).toEqual([5]);
                  });

                  describe("let's try the 'reduce' way", () => {

                    describe("which require some adaptations of 'push' function to work !", () => {

                      // remember pop signature ?
                      // pop : StackAlterationF

                      // 'push' must be aligned with 'pop' signature
                      // hence, currified and must return the correct tuple
                      const push_ =
                        (n: number): StackAlterationF =>
                          (stack: NumberStack) =>
                            [Maybe.Empty(), stack.concat([n])]; // obviously, push doesn't give us anything

                      it(`which works quite ~'nicely'...
                        It's more scalable because we can stack other transformations
                        in the Array. But we are not done yet !`, () => {

                        const numbers: NumberStack = [];
                        const initialDatas: MaybeNumberAndNumberStack = [
                          Maybe.Empty(),
                          numbers
                        ];

                        const [poppedAgain, numbersAfterPoppedAgain] =
                          [
                            push_(5),
                            push_(6),
                            pop
                          ].reduce((memo: MaybeNumberAndNumberStack, next: StackAlterationF) =>
                              next(memo[1]),
                            initialDatas
                          );

                        expect(poppedAgain.getOrElse(SHOULD_NOT_HAPPEN)).toBe(6);
                        expect(numbersAfterPoppedAgain).toEqual([5]);

                      });

                      describe("can we do better ?", () => {

                        describe("why not play with a Maybe ?", () => {

                          it(`well, it works ! But the semantic is not that good...
                           And we are still fiddling with state all along ((_[1])). `, () => {

                            const numbers: NumberStack = [];
                            const initialDatas: MaybeNumberAndNumberStack = [
                              Maybe.Empty(),
                              numbers
                            ];

                            const [num, end] =
                              Maybe.fromNullable(
                                initialDatas
                              ).map(_ =>
                                push_(5)(_[1])
                              ).map(_ =>
                                push_(6)(_[1])
                              ).map(_ =>
                                pop(_[1])
                              ).get() as MaybeNumberAndNumberStack;

                            expect(num.getOrElse(SHOULD_NOT_HAPPEN)).toBe(6);
                            expect(end).toEqual([5]);

                          });

                        });

                        describe(`Maybe was an improvement because of his 'Monadic' nature : capability to compose functions in a context.
                                But 'Maybe' is for context that **may be empty**... `, () => {

                          describe(`What about a context that **contains an alterable state** ?
                            Look, each 'map' above has the same 'shape' of execution. `, () => {

                            describe("Let's go for it !", () => {

                              // BEFORE : a little summary

                              // We are working on an **immutable data structure**
                              //  ImmutableData

                              // Structure we can modify (adding, removing elements)
                              // and each modification :
                              //  - give us something in return, extracted from that ImmutableData (yes, even a Maybe.Empty is something)
                              //  - give us the ImmutableData, **updated**

                              // Altering function result : MaybeNumberAndNumberStack
                              // Altering function        : StackAlterationF
                              //                            NumberStack => MaybeNumberAndNumberStack

                              //

                              describe("but before, let's create another example to fix the ideas", () => {

                                describe("how about a 'Candy dispenser' example ?", () => {

                                  enum Inputs {
                                    INSERT_COIN,
                                    TURN
                                  }

                                  class Machine {
                                    constructor(readonly unlocked: boolean,
                                                readonly candies: number,
                                                readonly coins: number) {
                                    }
                                  }

                                  const perform = (input: Inputs) =>
                                    (onMachine: Machine) => {


                                      // No candy ? Do nothing.
                                      if (onMachine.candies === 0) // case (_, Machine(_, 0, _)) => s
                                        return onMachine;

                                      if (input === Inputs.INSERT_COIN) {

                                        // Insert coin on a machine with a coin
                                        // Do nothing.
                                        if (onMachine.unlocked) { // case (Coin, Machine(false, _, _)) => s
                                          return onMachine;
                                        }

                                        // case (Coin, Machine(true, candy, coin)) =>
                                        //   Machine(false, candy, coin + 1)
                                        return new Machine(true, onMachine.candies, onMachine.coins + 1);

                                      } else if (input === Inputs.TURN) {

                                        // case (Turn, Machine(true, _, _)) => s
                                        // Turn handle on a unlocked machine
                                        // Do nothing.
                                        if (!onMachine.unlocked) {
                                          return onMachine;
                                        }

                                        // case (Turn, Machine(false, candy, coin)) =>
                                        //   Machine(true, candy - 1, coin)
                                        return new Machine(false, onMachine.candies - 1, onMachine.coins);
                                      } else {
                                        return onMachine;
                                      }

                                    }

                                  it(" that works as expected", () => {
                                    const machine = new Machine(false, 5, 2);
                                    const updated1 = perform(Inputs.INSERT_COIN)(machine);
                                    expect(updated1.unlocked).toBe(true);
                                    expect(updated1.coins).toBe(3);
                                    expect(updated1.candies).toBe(5);

                                    const updated2 = perform(Inputs.INSERT_COIN)(updated1);
                                    expect(updated2.unlocked).toBe(true);
                                    expect(updated2.coins).toBe(3);
                                    expect(updated2.candies).toBe(5);

                                    const updated3 = perform(Inputs.TURN)(updated1);
                                    expect(updated3.unlocked).toBe(false);
                                    expect(updated3.coins).toBe(3);
                                    expect(updated3.candies).toBe(4);
                                  });

                                  describe("so far so good... but ... where are the coins and the candies ???", () => {


                                  });

                                });

                              });

                              describe("let's name that Monad 'State'", () => {

                                class State<S, A> {
                                  constructor(readonly run: (initialState: S) => [A, S]) {
                                  }

                                }

                              });

                            });

                          });

                        });

                      });

                    });

                  });

                });

              });

            });

          });

        });

      });

    });

  });

});
