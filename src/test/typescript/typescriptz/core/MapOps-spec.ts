import {ListOps} from "../../../../../src/main/typescript/typescriptz/core/ListOps";
import {Tuples} from "../../../../../src/main/typescript/typescriptz/core/Tuples";
import {Maybe} from "../../../../main/typescript/typescriptz/core/Maybe";
import {MapOps} from "../../../../main/typescript/typescriptz/core/MapOps";


describe("MapOps", () => {

  it(" maybeGet works", () => {
    const m = new Map([
      ["a", 1],
      ["b", 2]
    ])

    expect(MapOps.maybeGet(m)("a").isDefined()).toBe(true);
    expect(MapOps.maybeGet(m)("c").isEmpty()).toBe(true);

  });

  it(" maybeGetOr works", () => {
    const m = new Map([
      ["a", 1],
      ["b", 2]
    ])

    expect(MapOps.maybeGetOr(m, () => 4807)("a")).toBe(1);
    expect(MapOps.maybeGetOr(m, () => 4807)("C")).toBe(4807);

  });

});
