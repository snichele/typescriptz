/* tslint:disable:typedef */

import {Try} from "../../../../main/typescript/typescriptz/core/Try";

describe("Try", () => {

  it("correctly convert a function call that can fail with an exception into an Either", () => {

    expect(Try.from<any>(() => {
      throw new Error("who ?");
    }).isLeft()).toBe(true);

    expect(Try.from<number>(() => {
      return 12345;
    }).getOrElse(() => -1)).toBe(12345);

  });

});
