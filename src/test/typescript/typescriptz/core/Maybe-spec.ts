import {api} from "../../../../main/typescript/typescriptz/api/typescriptz-api";
import {Maybe} from "../../../../main/typescript/typescriptz/core/Maybe";
import "../../../../main/typescript/typescriptz/jest.extensions";

const returnTrue = (a) => true;
const stringMatch = (x) => (y) => x === y;
const K = (x) => (y) => x;

describe("Maybe", () => {

  describe("construction", () => {

    it("must create a Just / Some from fromNullable(value)", () => {
      expect(Maybe.fromNullable("toto")).toBeDefined();
      expect(Maybe.fromNullable("toto").isJust()).toBe(true);
    });

    it("must create a Empty / None from fromNullable(null) or fromNullable(undefined)", () => {
      expect(Maybe.fromNullable(null)).toBeDefined();
      expect(Maybe.fromNullable(undefined)).toBeDefined();
      expect(Maybe.fromNullable(null).isEmpty()).toBe(true);
      expect(Maybe.fromNullable(undefined).isEmpty()).toBe(true);
    });

    it("must create a Empty / None from arrays of const ious sizes according to tsdoc", () => {
      console.error(Maybe.headOption([]));
      expect(Maybe.headOption([]).isEmpty()).toBe(true);
      expect(Maybe.headOption(["toto"]).isDefined()).toBe(true);
      expect(Maybe.headOption(["toto", "tata"]).isDefined()).toBe(true);
      expect(Maybe.headOption(["toto", "tata"]).get()).toBe("toto");
    });

  });

  describe("isXXXX", () => {

    it("works for isDefined, isJust and isEmpty", () => {
      expect(Maybe.Just("2").isDefined()).toBeTruthy();
      expect(Maybe.Just("2").isEmpty()).toBeFalsy();
      expect(Maybe.Just("2").isJust()).toBeTruthy();

      expect(Maybe.Empty().isDefined()).toBeFalsy();
      expect(Maybe.Empty().isEmpty()).toBeTruthy();
      expect(Maybe.Empty().isJust()).toBeFalsy();
    });

  });

  describe("exists()", () => {

    it("must return false for an Empty", () => {
      expect(Maybe.fromNullable(undefined).exists(returnTrue)).toBeFalsy();
      expect(Maybe.fromNullable(null).exists(returnTrue)).toBeFalsy();
    });

    it("must return true for a Matching Just", () => {
      expect(Maybe.fromNullable("toto").exists(stringMatch("toto"))).toBe(true);
    });

    it("must return false for a non Matching Just", () => {
      expect(Maybe.fromNullable("toto").exists(stringMatch("tata"))).toBe(false);
    });
  });

  describe("filter()", () => {

    it("must return empty for an Empty", () => {
      expect(Maybe.fromNullable(undefined).filter(returnTrue)).toEqual(Maybe.Empty());
      expect(Maybe.fromNullable(null).filter(returnTrue)).toEqual(Maybe.Empty());
    });

    it("must return the original Just for a Matching Just", () => {
      expect(Maybe.fromNullable("toto").filter(returnTrue)).toEqual(Maybe.Just("toto"));
    });

    it("must return empty for a non Matching Just", () => {
      expect(Maybe.fromNullable("toto").filter(stringMatch("tata"))).toEqual(Maybe.Empty());
    });

  });

  describe("filterNot()", () => {

    it("must return empty for an Empty", () => {
      expect(Maybe.fromNullable(undefined).filterNot(returnTrue)).toEqual(Maybe.Empty());
      expect(Maybe.fromNullable(null).filterNot(returnTrue)).toEqual(Maybe.Empty());
    });

    it("must return the original Just for a NON Matching Just", () => {
      expect(Maybe.fromNullable("toto").filterNot(stringMatch("tata"))).toEqual(Maybe.Just("toto"));
    });

    it("must return empty for a matching Just", () => {
      expect(Maybe.fromNullable("toto").filterNot(stringMatch("toto"))).toEqual(Maybe.Empty());
    });

  });

  describe("map()", () => {

    it("must return Empty for an Empty", () => {
      expect(Maybe.Empty().map(K("whatever")).isEmpty()).toBe(true);
    });

    it("must correctly apply for a Just", () => {
      expect(Maybe.Just("Bob").map((s) => "Hello " + s)).toEqual(
        Maybe.Just("Hello Bob")
      );
    });

    it("must correctly apply for a Just with a null returning lambda (return a Just wrapped null !)", () => {
      expect(Maybe.Just("Bob").map(K(null))).toEqual(
        Maybe.Just(null)
      );
    });

  });

  describe("flatMap()", () => {

    it("must behaves correctly (too lazy t oslice this in three)", () => {


      const result: api.Maybe<string> = Maybe.Just("toto").flatMap((s: string) => Maybe.Just(s.toUpperCase()));
      expect(result.isEmpty()).toBe(false);
      expect(result.get()).toBe("TOTO");

      const result2 = Maybe.Empty<string>().flatMap((s: string) => Maybe.Just(s.toUpperCase()));
      expect(result2.isEmpty()).toBe(true);

      const result3 = Maybe.Just("toto").flatMap((s: string) => Maybe.Empty<string>());
      expect(result3.isEmpty()).toBe(true);

    });


  });

  describe("getOrElse()", () => {

    it("must return the wrapped value if there is some", () => {
      expect(Maybe.Just("Bob").getOrElse(() => "whatever")).toBe("Bob");
    });

    it("must return the default value if there is no wrapped value", () => {
      expect(Maybe.Empty().getOrElse(() => "default")).toBe("default");
    });

  });

  describe("orElse()", () => {

    it("must return a Maybe with the wrapped value if there is some", () => {
      expect(Maybe.Just("Bob").orElse(Maybe.Just("whatever"))).toEqual(Maybe.Just("Bob"));
    });

    it("must return the provided Maybe if there is no wrapped value", () => {
      expect(Maybe.Empty().orElse(Maybe.Just("whatever"))).toEqual(Maybe.Just("whatever"));
    });

  });

  describe("get()", () => {

    it("must return the wrapped value if there is some", () => {
      expect(Maybe.Just("Bob").get()).toBe("Bob");
    });

    it("must return undefined if there is no wrapped value", () => {
      expect(Maybe.Empty().get()).toBeUndefined();
    });

  });

  describe("fold()", () => {

    it("must correctly apply the function in the content of a just", () => {
      expect(Maybe.Just("Bob").fold((x: string) => x.toUpperCase(), () => "whatever")).toBe("BOB");
    });

    it("must return default value if the content is empty", () => {
      expect(Maybe.Empty().fold((x: string) => x.toUpperCase(), () => "whatever")).toBe("whatever");
    });


  });

  describe("equals()", () => {

    it("must works", () => {
      expect(Maybe.Empty().equals(Maybe.Empty())).toBe(true);
      expect(Maybe.Just(1).equals(Maybe.Empty())).toBe(false);
      expect(Maybe.Just(1).equals(Maybe.Just(1))).toBe(true);
      expect(Maybe.Empty().equals(Maybe.Just(1))).toBe(false);
      expect(Maybe.Just("toto").equals(Maybe.Just("TOTO"), (a, b) => a.toLowerCase() === b.toLowerCase())).toBe(true);
    });

  });

  describe("typeclasses nature", () => {

    it("has applicative capability", () => {

      const maybeFunction = Maybe.Just((x: string) => x.toUpperCase());
      const maybeValue = Maybe.Just("toto");

      const r = maybeFunction.asApplicative().ap(maybeFunction)(maybeValue);
      expect(r).toEqual(Maybe.Just("TOTO"));
    });

  });

  describe("what sorcery ", () => {

    it(" is ?", () => {

      const maybeValue = Maybe.Just(new Date(123456789));

      expect(maybeValue.get()).toEqual(new Date(123456789));
    });

    it(" isNaN ?", () => {

      const maybeValue = Maybe.Just(parseFloat("toto")).filterNot(isNaN);

      expect(maybeValue.isEmpty()).toBe(true);
    });

  });

  describe(" when dealing with Typeclasses", () => {

    describe(" can turn a maybe to an applicative ", () => {

      const app: api.Applicative<any> = Maybe.Just("toto").asApplicative();

      it("that can point ", () => {
        const pointed: api.Maybe<string> = <api.Maybe<string>>app.point(() => "tata");
        expect(app).toBeDefined();
        expect(pointed.get()).toBe("tata");
      });

    });

  });

  describe("have custom expect matchers (jest in mind)", () => {

    it(" definedAndContains ", () => {

      const maybeValue = Maybe.Just("toto");

      expect(maybeValue.exists(_ => _ === "toto")).toBe(true);

    });

  });

});
