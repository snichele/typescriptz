import {api} from "../../../../../src/main/typescript/typescriptz/api/typescriptz-api";

describe("Predicates", () => {


  it(" defines a nullOrEmptyPredicate working function ", () => {
    expect(api.Predicates.nullOrEmptyPredicate(null)).toBeTruthy();
    expect(api.Predicates.nullOrEmptyPredicate(void 0)).toBeTruthy();
    expect(api.Predicates.nullOrEmptyPredicate("")).toBeFalsy();
  });

  it(" defines a nullOrEmptyStringPredicate working function ", () => {
    expect(api.Predicates.nullOrEmptyStringPredicate(null)).toBeTruthy();
    expect(api.Predicates.nullOrEmptyStringPredicate(void 0)).toBeTruthy();
    expect(api.Predicates.nullOrEmptyStringPredicate("")).toBeTruthy();
  });

});
