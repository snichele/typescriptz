import {Tuples} from "../../../../../src/main/typescript/typescriptz/core/Tuples";

describe("Tuple2", () => {

  describe("construction", () => {

    it(" works and content is accessible ", () => {

      const  tuple2 = Tuples.Tuple2("blip", 25);

      expect(tuple2).toBeDefined();
      expect(tuple2.first()).toEqual("blip");
      expect(tuple2.second()).toEqual(25);

    });

  });

  describe("bimap()", () => {

    it(" works ", () => {
      const  tuple2 = Tuples.Tuple2("blip", 25);

      expect(
        tuple2.bimap(
          (v1: string) => v1.toUpperCase(),
          (v2: number) => v2 * 10
        )
      ).toEqual(
        Tuples.Tuple2("BLIP", 250)
      );

    });

  });
});
