import "../../../../main/typescript/typescriptz/globals.extensions";

describe("Globals definitions", () => {

  describe("augment basic types", () => {

    describe("to be lifted as Maybe easily with xxx.just() or Type.fromNullable(...) ", () => {

      it("for string", () => {

        expect("".just().isDefined()).toBe(true);
        expect(String.fromNullable("").isDefined()).toBe(true);
        expect(String.fromNullable(void 0).isEmpty()).toBe(true);

      });

      it("for number", () => {

        expect((2).just().isDefined()).toBe(true);
        expect(Number.fromNullable(2).isDefined()).toBe(true);
        expect(Number.fromNullable(void 0).isEmpty()).toBe(true);

      });

      it("for boolean", () => {

        expect((true).just().isDefined()).toBe(true);
        expect(Boolean.fromNullable(false).isDefined()).toBe(true);
        expect(Boolean.fromNullable(void 0).isEmpty()).toBe(true);

      });

    });

  });

});
