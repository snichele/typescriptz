import {api} from "../../../../main/typescript/typescriptz/api/typescriptz-api";
import {Either} from "../../../../main/typescript/typescriptz/core/Either";

/* tslint:disable:typedef */

describe("Either", () => {

  const toUpper = (s: string) => s.toUpperCase();
  const negateNum = (x: number) => -x;
  const toStr = (x: any) => "" + x;
  const id = (x: any) => x;

  describe("Left", () => {

    it("behaves as expected", () => {

      const l: api.Either<number, string> = Either.Left(4807);

      expect(l.isLeft()).toBeTruthy();
      expect(l.isRight()).toBeFalsy();
      expect(l.fold<string>((x) => {
        return "" + negateNum(x);
      }, toUpper)).toEqual("-4807");
      expect(l.getOrElse(() => "default")).toEqual("default");
      expect(l.leftMap((a) => "--" + a).fold(toStr, toStr)).toEqual("--4807");
      expect(l.map((a) => "--" + a).fold(toStr, toStr)).toEqual("4807");
      expect(l.flatMap<string>((a) => Either.Right("wow " + a)).fold(toStr, toStr)).toEqual("4807");
      expect(l.orElse(() => Either.Right<number, string>("foobar")).fold(toStr, toStr)).toEqual("foobar");
      expect(l.valueOr((x) => "" + (x + 1000))).toEqual("5807");
      expect(l.match({
        left: (left) => "LEFT" + left,
        right: (right) => "RIGHT" + right
      })).toEqual("LEFT4807");
    });

  });

  describe("Right", () => {

    it("behaves as expected", () => {

      // left could be an error number
      const r: api.Either<number, string> = Either.Right("foo");

      expect(r.isLeft()).toBeFalsy();
      expect(r.isRight()).toBeTruthy();
      expect(r.fold<string>((x) => {
        return "" + negateNum(x);
      }, toUpper)).toEqual("FOO");
      expect(r.getOrElse(() => "default")).toEqual("foo");
      expect(r.leftMap((a) => "--" + a).fold(toStr, toStr)).toEqual("foo");
      expect(r.map((a) => "--" + a).fold(toStr, toStr)).toEqual("--foo");
      expect(r.flatMap<string>((a) => Either.Right("wow " + a)).fold(toStr, toStr)).toEqual("wow foo");
      // a right, then a left
      expect(r.flatMap<string>((a) => Either.Left<number, string>(-1)).fold(id, id)).toEqual(-1);
      expect(r.orElse(() => Either.Right<number, string>("foobar")).fold(toStr, toStr)).toEqual("foo");
      expect(r.valueOr((x) => {
        return "" + (x + 1000);
      })).toEqual("foo");

      expect(r.swap().leftMap((a) => {
        return "--" + a;
      }).fold(toStr, toStr)).toEqual("--foo");

      expect(r.match({
        left: (left) => {
          return "LEFT" + left;
        },
        right: (right) => {
          return "RIGHT" + right;
        }
      })).toEqual("RIGHTfoo");

    });

  });

});
