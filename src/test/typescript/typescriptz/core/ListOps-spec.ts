import {ListOps} from "../../../../../src/main/typescript/typescriptz/core/ListOps";
import {Tuples} from "../../../../../src/main/typescript/typescriptz/core/Tuples";
import {Maybe} from "../../../../main/typescript/typescriptz/core/Maybe";


class Toto {
  static xValue = (t: Toto) => t.x;

  constructor(public x: string, public y: number) {
  }
}

describe("ListOps", () => {

  it(" maybeHead works", () => {
    const noDatas = [];
    const oneData = ["foo"];
    const moreDatas = ["bar", "baz"];

    expect(ListOps.maybeHead(null).isEmpty()).toBe(true);
    expect(ListOps.maybeHead(noDatas).isEmpty()).toBe(true);

    expect(ListOps.maybeHead(oneData).isJust()).toBe(true);
    expect(ListOps.maybeHead(oneData).exists((x) => x === "foo")).toBe(true);

    expect(ListOps.maybeHead(moreDatas).isJust()).toBe(true);
    expect(ListOps.maybeHead(moreDatas).exists((x) => x === "bar")).toBe(true);

    Maybe.Just(["A", "B", "C"]).flatMap(ListOps.maybeHead).getOrElse(() => "NONE");
  });

  it(" zip works", () => {
    const arr1 = ["a", "b", "c"];
    const arr2 = [1, 2];
    const arr3 = [true, true, false, true];

    const zipped = ListOps.zip(arr1, arr2);
    const zipped2 = ListOps.zip(arr2, arr3);

    expect(zipped).toEqual([
      Tuples.Tuple2("a", 1),
      Tuples.Tuple2("b", 2)
    ]);

    expect(zipped2).toEqual([
      Tuples.Tuple2(1, true),
      Tuples.Tuple2(2, true)
    ]);

  });

  it(" lengthMatcher", () => {
    let emptyOk = false, ifMoreThanOne = false, ifOne = false;

    ListOps.lengthMatcher([],
      () => emptyOk = true,
      () => ifOne = true,
      () => ifMoreThanOne = true
    );

    expect(emptyOk).toBeTruthy();
    expect(ifMoreThanOne).toBeFalsy();
    expect(ifOne).toBeFalsy();

    emptyOk = false;
    ifMoreThanOne = false;
    ifOne = false;

    ListOps.lengthMatcher([1],
      () => emptyOk = true,
      () => ifOne = true,
      () => ifMoreThanOne = true
    );
    expect(emptyOk).toBeFalsy();
    expect(ifMoreThanOne).toBeFalsy();
    expect(ifOne).toBeTruthy();

    emptyOk = false;
    ifMoreThanOne = false;
    ifOne = false;

    ListOps.lengthMatcher([1, 2],
      () => emptyOk = true,
      () => ifOne = true,
      () => ifMoreThanOne = true
    );
    expect(emptyOk).toBeFalsy();
    expect(ifMoreThanOne).toBeTruthy();
    expect(ifOne).toBeFalsy();

  });

  it(" groupBy", () => {

    const ts = [
      new Toto("a", 10),
      new Toto("a", 12),
      new Toto("b", 15)
    ];

    expect(
      ListOps.groupBy([], Toto.xValue).length
    ).toEqual(0);

    expect(
      ListOps.groupBy(ts, Toto.xValue).length
    ).toEqual(2);

  });

  it(" onlyOneThatMatch", () => {

    const ts = [
      new Toto("a", 10),
      new Toto("a", 12),
      new Toto("b", 15)
    ];

    expect(
      ListOps.onlyOneThatMatch([], _ => _.x !== null)
    ).toEqual(Maybe.Empty());

    expect(
      ListOps.onlyOneThatMatch(ts, _ => _.x !== null)
    ).toEqual(Maybe.Empty());

    expect(
      ListOps.onlyOneThatMatch(ts, _ => _.y === 12).map(_ => _.x).getOrElse(() => "Failed !")
    ).toEqual("a");

  });

});
