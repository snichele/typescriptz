import {FunctionsOps} from "../../../../main/typescript/typescriptz/core/FunctionsOps";

/* tslint:disable:typedef */

describe("FunctionsOps", () => {

  describe("functions composition", () => {

    it("works using the DSL", () => {

      const  add1 = function (x) { return x + 1; };
      const  mult2 = function (x) { return x * 2; };
      const  square = function (x) { return x * x; };
      const  negate = function (x) { return -x; };

      const  f = FunctionsOps.first(add1).then(mult2).andThen(square).andThen(negate).finish();

      expect(f(0)).toEqual(-4);
    });

  });

});
