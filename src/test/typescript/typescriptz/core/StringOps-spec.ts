import {assertForall, elements, pair, tuple, string, nestring, char, number, suchthat} from "jsverify";
import {StringOps} from "../../../../main/typescript/typescriptz/core/StringOps";
import longerThan = StringOps.longerThan;
import textBeforeToken = StringOps.textBeforeToken;
import isEmpty = StringOps.isEmpty;
import between = StringOps.between;
import capitalize = StringOps.capitalize;

describe("StringOps permits me to", () => {

  it(" longerThan - test a string length", () => {
    assertForall(pair(string, number), ([s, num]) =>
      longerThan(num)(s) === s.length > num
    );
  });

  it(" textBeforeToken - extract text before a token", () => {
    assertForall(pair(string, char), ([s, token]) =>
      textBeforeToken(token)(s) === ""
      || textBeforeToken(token)(s).length > 0
    );
  });

  it(" isEmpty - test if a string is 'empty', does not contains printable chars", () => {
    assertForall(suchthat(nestring, s => s.trim().length === s.length), s =>
      !isEmpty(s.trim()) // trim() added because 'nestring' return things like '\n' ...
    );
  });

  it(" between - fetch a part of a string", () => {
    assertForall(tuple([string, number, number]), ([s, start, end]) =>
      (between(start, end)(s) === "") || (s.indexOf(between(start, end)(s)) !== -1)
    );
  });

  it(" capitalize - Turn first letter into a capital", () => {
    expect(capitalize("")).toBe("");
    expect(capitalize("a")).toBe("A");
    expect(capitalize("A")).toBe("A");
    expect(capitalize("abc")).toBe("Abc");
    expect(capitalize("abC")).toBe("Abc");
  });

});
