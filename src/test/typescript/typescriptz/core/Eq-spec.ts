import {array, assertForall, string} from "jsverify";
import {Eq} from "../../../../main/typescript/typescriptz/core/Eq";
import WithCodeEq = Eq.WithCodeEq;
import areEquals = Eq.WithCodeEq.areEquals;
import isPairEquals = Eq.WithCodeEq.isPairEquals;
import getCode = Eq.WithCodeEq.getCode;
import findInArray = Eq.WithCodeEq.findInArray;

describe("Eq interface associated namespace", () => {

  describe("provides a WithCodeEq implementation, based on 'code' equality", () => {

    describe("which can", () => {

      it("be instantiated with any non empty string as a 'code'", () => {
        assertForall(string, (s) =>
          new WithCodeEq(s) !== void 0
        );
      });

      it("be compared for equality", () => {

        assertForall(string, (s) =>
          areEquals(
            new WithCodeEq(s)
          )(
            new WithCodeEq(s)
          )
        );
        assertForall(string, (s) =>
          isPairEquals([
            new WithCodeEq(s),
            new WithCodeEq(s)
          ])
        );

      });

      it("be queried for code", () => {
        assertForall(string, (s) =>
          getCode(new WithCodeEq(s)) === s
        );
      });

      it("be found in an array of instances", () => {
        assertForall(array(string), (strings) =>
          (strings.length > 0) ? findInArray(
            strings[0]
            )(
            strings.map(WithCodeEq.of)
            ).isDefined() :
            true
        );

      });

    });

  });

});
