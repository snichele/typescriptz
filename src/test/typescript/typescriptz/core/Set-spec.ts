import {Set} from "../../../../../src/main/typescript/typescriptz/core/Set";
import {api} from "../../../../../src/main/typescript/typescriptz/api/typescriptz-api";

class StubEqualatable implements api.Eq {

  constructor(public id: string, public foo: number, public bar: boolean) {
  }

  equals(another: api.Eq): boolean {
    if (another instanceof StubEqualatable) {
      return this.id === another.id;
    } else {
      return false;
    }
  }
}

const emptySet = Set.fromArray([]);
const numberSet: api.Set<number> = Set.fromArray([1, 2, 3]);
const numberSetCreatedWithDuplicatesInSourceJSArray = Set.fromArray([1, 2, 2, 3, 3]);
const stringSetCreatedWithDuplicatesInSourceJSArray: api.Set<string> = Set.fromArray(["a", "a", "b", "c", "d", "c", "d"]);
const eqSet = Set.fromArray([
  new StubEqualatable("xxx", 25, false),
  new StubEqualatable("yyy", 25, false)
]);
const eqSetCreatedWithDuplicatesInSourceJSArray: api.Set<StubEqualatable> = Set.fromArray([
  new StubEqualatable("xxx", 25, false),
  new StubEqualatable("yyy", 25, false),
  new StubEqualatable("xxx", 125, true),
]);

describe("Set type", () => {

  describe("construction", () => {

    it(" correctly deduplicates original provided array ", () => {
      expect(emptySet).toBeDefined();

      expect(numberSet).toBeDefined();
      expect(numberSet.length).toEqual(3);

      expect(numberSetCreatedWithDuplicatesInSourceJSArray).toBeDefined();
      expect(numberSetCreatedWithDuplicatesInSourceJSArray.length).toEqual(3);

      expect(stringSetCreatedWithDuplicatesInSourceJSArray).toBeDefined();
      expect(stringSetCreatedWithDuplicatesInSourceJSArray.length).toEqual(4);

      expect(eqSet).toBeDefined();
      expect(eqSet.length).toEqual(2);

      expect(eqSetCreatedWithDuplicatesInSourceJSArray).toBeDefined();
      expect(eqSetCreatedWithDuplicatesInSourceJSArray.length).toEqual(2);
    });

    it(" is not affected by original provided array modification after construction ", () => {
      const array = [1, 2, 3];
      const anotherNumberSet = Set.fromArray(array);
      expect(anotherNumberSet.length).toEqual(3);
      array.push(5);
      array.push(25);
      expect(anotherNumberSet.length).toEqual(3);
      expect(array.length).toEqual(5);
    });

  });

  it(" does not create duplicates when adding items", () => {

    expect(numberSet.add(1234).length).toEqual(4);
    expect(numberSet.add(1).length).toEqual(3);

    expect(stringSetCreatedWithDuplicatesInSourceJSArray.add("foo").length).toEqual(5);
    expect(stringSetCreatedWithDuplicatesInSourceJSArray.add("a").length).toEqual(4);

    expect(eqSet.add(new StubEqualatable("NEW", 25, false)).length).toEqual(3);
    expect(eqSet.add(new StubEqualatable("yyy", 25, false)).length).toEqual(2);

  });

  it(" correctly remove item", () => {

    expect(numberSet.remove(1245).length).toEqual(3);
    expect(numberSet.remove(1).length).toEqual(2);

    expect(stringSetCreatedWithDuplicatesInSourceJSArray.remove("foo").length).toEqual(4);
    expect(stringSetCreatedWithDuplicatesInSourceJSArray.remove("a").length).toEqual(3);

    expect(eqSet.remove(new StubEqualatable("NEW", 25, false)).length).toEqual(2);
    expect(eqSet.remove(new StubEqualatable("yyy", 25, false)).length).toEqual(1);

  });

  it(" can be converted back to js array (which is a clone of the Set backing array to prevent hacking)", () => {
    // aquire a clone of the array
    const toArray = numberSet.toArray();
    // check the length
    expect(toArray.length).toEqual(3);
    // modify it !
    toArray.push(5);
    // array clone modified ?
    expect(toArray.length).toEqual(4);
    // but set not !
    expect(numberSet.length).toEqual(3);
  });

  describe("correctly delegate to underlying array methods, ie ", () => {

    it(" union ", () => {
      const a1: api.Set<number> = Set.fromArray([1, 2, 3]);
      const a2: api.Set<number> = Set.fromArray([3, 4, 1, 5]);
      expect(a1.union(a2).toArray()).toEqual([1, 2, 3, 4, 5]);
    });

    it(" join ", () => {
      expect(numberSet.join(":")).toEqual("1:2:3");
    });

    it(" reverse ", () => {
      expect(numberSet.reverse().toArray()).toEqual([3, 2, 1]);
    });

    it(" slice ", () => {
      expect(numberSet.slice(0, 1).toArray()).toEqual([1]);
    });

    it(" sort ", () => {
      expect(numberSet.sort((x: number, y: number) => {
        return y - x;
      }).toArray()).toEqual([3, 2, 1]);

      expect(stringSetCreatedWithDuplicatesInSourceJSArray.sort((x: string, y: string) => {
        return (x > y) ? -1 : 1;
      }).toArray()).toEqual(["d", "c", "b", "a"]);

      expect(eqSetCreatedWithDuplicatesInSourceJSArray.sort((x: StubEqualatable, y: StubEqualatable) => {
        return (x.id > y.id) ? -1 : 1;
      }).toArray()).toEqual([
        new StubEqualatable("yyy", 25, false),
        new StubEqualatable("xxx", 25, false)
      ]);
    });

    it(" indexOf ", () => {
      expect(numberSet.indexOf(1)).toEqual(0);
      expect(stringSetCreatedWithDuplicatesInSourceJSArray.indexOf("b")).toEqual(1);
      expect(eqSetCreatedWithDuplicatesInSourceJSArray.indexOf(new StubEqualatable("xxx", 25, false))).toEqual(0);
      expect(eqSetCreatedWithDuplicatesInSourceJSArray.indexOf(new StubEqualatable("yyy", 25, false))).toEqual(1);
    });

    it(" every", () => {
      expect(numberSet.every((_: number) => _ > 0)).toBeTruthy();
      expect(stringSetCreatedWithDuplicatesInSourceJSArray.every((_: string) => _ !== "toto")).toBeTruthy();
      expect(eqSetCreatedWithDuplicatesInSourceJSArray.every((_: StubEqualatable) => _.id !== "toto")).toBeTruthy();
    });

    it(" some", () => {
      expect(numberSet.some((_: number) => _ === 2)).toBeTruthy();
      expect(stringSetCreatedWithDuplicatesInSourceJSArray.some((_: string) => _ === "d")).toBeTruthy();
      expect(eqSetCreatedWithDuplicatesInSourceJSArray.some((_: StubEqualatable) => _.id === "xxx")).toBeTruthy();
    });

    it(" forEach", () => {
      let counter = 0;
      numberSet.forEach((_: number) => counter++);
      expect(counter).toEqual(3);
      stringSetCreatedWithDuplicatesInSourceJSArray.forEach((_: string) => counter++);
      expect(counter).toEqual(7);
    });

    describe(" map ", () => {

      it(" as expected", () => {
        expect(numberSet.map((_: number) => "Number" + _).toArray()).toEqual(["Number1", "Number2", "Number3"]);
      });

      it("with no doublons after a map", () => {
        expect(numberSet.map((_: number) => 1).toArray()).toEqual([1]);
      });

    });

    it(" filter ", () => {
      expect(numberSet.filter((_: number) => _ > 0).toArray()).toEqual([1, 2, 3]);
    });

    it(" reduce ", () => {
      expect(numberSet.reduce((_1: number, _2: number) => _1 + _2, 0)).toEqual(6);
    });

    it(" reduceRight ", () => {
      expect(numberSet.reduceRight((_1: number, _2: number) => _1 + _2, 0)).toEqual(6);
    });

    it(" head", () => {
      expect(numberSet.head().exists(n => n === 1)).toBe(true);
    });

    it(" last", () => {
      expect(numberSet.tail()).toEqual(Set.fromArray([2, 3]));
    });

    it(" last", () => {
      expect(numberSet.last().exists(n => n === 3)).toBe(true);
    });

  });

  describe("correctly works for the 'non-standard' methods , ie ", () => {

    it(" filterNot ", () => {
      expect(numberSet.filterNot((_: number) => _ > 1).toArray()).toEqual([1]);
    });

    it(" find ", () => {
      expect(numberSet.find((_: number) => _ > 1).getOrElse(() => -1)).toEqual(2);
      expect(eqSetCreatedWithDuplicatesInSourceJSArray.find((_: StubEqualatable) => _.id !== "xxx").getOrElse(() => null)).toBeDefined();
    });

  });
});
