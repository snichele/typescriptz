import {Function1} from "../../../../main/typescript/typescriptz/core/Function1";

describe("Function1", () => {

  const parse = (s: string): number => parseInt(s, 10);
  const mult10 = (x: number): number => x * 10;
  const id = <A>(a: A) => a;

  describe("map must respect functor laws", () => {

    it("identity", () => {
      const composable = new Function1(parse);
      expect(
        composable.andThen(id).apply("100")
      ).toBe(
        composable.apply("100")
      )
    });

    it("composition", () => {
      const composable = new Function1(parse).andThen(mult10);
      const composedManually = x => mult10(parse(x))
      expect(
        composable.apply("10")
      ).toBe(
        composedManually("10")
      )
    });

  });

});