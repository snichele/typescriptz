import {filter, map} from "rxjs/operators";
import {Maybe} from "../../../../main/typescript/typescriptz/core/Maybe";
import {Observable, of} from "rxjs";

class Stock {
  constructor(public readonly quantity: number,
              public readonly symbol: string) { }
}

class Pricing {
  constructor(public readonly symbol: string,
              public readonly atDate: Date,
              public readonly value: number) { }
}

class StockService {
  stocks: Array<Stock> = [
    new Stock(10, "XXX"),
    new Stock(20, "YYY"),
    new Stock(30, "ZZZ")
  ];
  getStocks = () => this.stocks;
}

/** Pour des raisons légales, le système de pricing */
class PricingService {
  pricings: Array<Pricing> = [
    new Pricing("XXX", new Date(100), 10),
    new Pricing("YYY", new Date(100), 20),
    new Pricing("ZZZ", new Date(100), 30)
  ];

  getPricings = () => this.pricings;

  getPricingsForDate = (d: Date) =>
    this.pricings.filter(_ => {
      return _.atDate.getDate() === d.getDate();
    })

  getPricingsForSymbol = (s: string) =>
    this.pricings.filter(_ => _.symbol === s)

  /** Get pricing for the symbol and date, otherwise, return null if not found. */
  getPricingForDateAndSymbol = (d: Date, s: string) =>
    this.pricings.filter(_ => _.symbol === s).filter(_ => _.atDate === d)

}

describe(`exemple d'utilisation : différents use cases...`, () => {

  const stockService = new StockService();
  const pricingService: PricingService = new PricingService();

  it(" je dois récupère une valeur, mais celle-ci peut-être inexistante", (hop: () => void) => {
    const pricing = pricingService.getPricingForDateAndSymbol(new Date(100), "XYZ");
    const array = of("hello", "world", "how", "are", "you", ".");

    const arrayCleaned =
      array.pipe(
        filter(_ =>
          _.length > 1
        ),
        map(_ =>
          _.toUpperCase()
        ),
        map(_ =>
          _.replace("=", ":")
        )
      );

    arrayCleaned.subscribe(_ => {
      console.info(_);
      hop();
    });
  });

});
