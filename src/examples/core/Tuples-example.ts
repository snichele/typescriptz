/// <reference path="../../main/typescript/typescriptz/Tuples.ts" />

// Let's say we have an array of a complex object

var objects = [
  {id: 1, name: 'foo'},
  {id: 2, name: 'bar'},
  {id: 3, name: 'baz'}
];

// and we need to check their validity

function checkObjects(arr: Array<any>) {
  //...
}

// what must this function return ?
// a bunch of 'true', 'false', 'false', etc ???
// original objects with another property setted 'isValid' ?

// It will depend on you use case ; but the simplest way to perform this
// is to return an array with the original objects, paired with the check status alongside.
// For creating 'Pairs' of datas, we use the Tuple2 class

import Tuples = typescriptz.core.Tuples;

var t = Tuples.Tuple2('first value', 'second value');

// then you can access the values with
t.first();
t.second();

// so checkObjects could become
function checkObjectsAgain(arr: Array<any>) {
  return arr.map((v) => Tuples.Tuple2(v, validate(v)));

  function validate(x) {
    // perform your check, and return true or false.
    return x !== void 0;
  }
}

// calling the function will gives you something like that :

var results = [
  Tuples.Tuple2({id: 1, name: 'foo'}, true),
  Tuples.Tuple2({id: 2, name: 'bar'}, true),
  Tuples.Tuple2({id: 3, name: 'baz'}, false)
];