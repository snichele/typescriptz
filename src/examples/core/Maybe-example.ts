/// <reference path="../../main/typescript/typescriptz/Maybe.ts" />

// Let's say we need to deal with a value that can potentially be null

// We have a service that can potentially return null...
module SomeOldStyleService {
  export function getSomething(byId: string) {
    return null;
  }
}

var a = SomeOldStyleService.getSomething('toto');
var finalResult;
if (a == null) {
  // treat the case of null
  finalResult = 0;
} else {
  // use a
  finalResult = a * a;
}

// That's some ugly procedural code, with a useless var.

// Try Maybe that manage the problem elegantly !

import Maybe = typescriptz.core.Maybe;

finalResult = Maybe.fromNullable(           // create the maybe...
  SomeOldStyleService.getSomething('toto')  // from something that can return null
).map((x) =>
  x + 5                                     // map this function on the content, only if it exists !
).getOrElse(() =>
    0                                       // then extract the result, or a default value if there was nothing.
);

// note that in Scala, it would have been written as
// finalResult = Maybe.fromNullable(SomeOldStyleService.getSomething('toto')).map(_ + 5).getOrElse(0)