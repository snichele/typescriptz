
export {};

declare namespace jest {

  interface Matchers<R> {
    /** Check that a tsz.Maybe is defined AND contains a value. */
    definedAndContains(expected: any): R
  }

}

expect.extend({

  definedAndContains(received: any, argument: any) {

    if (received["match"] === null) {
      return ({
        message: () => " 'definedAndContains' should be applied to a typescriptz Maybe !",
        pass: false
      });
    } else {
      return received.match({
        empty: () => ({message: () => "Received value was empty !", pass: false}),
        just: (x: any) => (x === argument) ?
          ({message: () => `Correctly contains ${x} compared with strict '==='`, pass: true}) :
          ({message: () => `Expected Maybe content '${argument}' is different (!==) of '${x}'`, pass: false})
      })
    }

  }

});

