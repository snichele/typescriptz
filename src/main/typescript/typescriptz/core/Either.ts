import {api} from "../api/typescriptz-api";

/**
 * api.Either related operations.
 *
 * @see {@link api.Either}
 */
export namespace Either {

  /** Construct a right disjunction value. */
  export function Right<A, B>(x: B): api.Either<A, B> {
    return newEither<A, B>(undefined, x);
  }

  /** Construct a left disjunction value. */
  export function Left<A, B>(x: A): api.Either<A, B> {
    return newEither<A, B>(x, undefined);
  }

  class EitherImpl<A, B> implements api.Either<A | undefined, B | undefined> {

    constructor(private a: A, private b: B) {
      if (a !== undefined && b !== undefined) {
        throw new Error("An Either cannot be constructed from a left AND a right value !");
      }
    }

    isLeft() {
      return (this.a !== undefined);
    }

    isRight() {
      return (this.b !== undefined);
    }

    fold<X>(l: (a: A) => X, r: (n: B) => X) {
      return (this.isLeft()) ? l(this.a) : r(this.b);
    }

    // in the not part of the ternary below, the casting to C,B is done to force the type.
    leftMap<C>(f: (a: A) => C): api.Either<C, B> {
      return (this.isLeft()) ? newEither(f(this.a), this.b) : newEither<C, B>(undefined, this.b);
    }

    map<D>(g: (b: B) => D): api.Either<A, D> {
      return (this.isRight()) ? newEither<A, D>(undefined, g(this.b)) : newEither<A, D>(this.a, undefined);
    }

    flatMap<C>(f: (b: B) => api.Either<A, C>): api.Either<A, C> {
      return (this.isRight()) ?
        f(this.b) :
        newEither<A, C>(this.a, undefined);
    }

    getOrElse(x: () => B): B {
      if (this.isRight()) {
        return this.b;
      } else {
        if (this.isLeft()) {
          return x();
        } else {
          return this.throwIllegalEitherState();
        }
      }

    }

    valueOr(x: (a: A) => B): B {
      if (this.isRight()) {
        return this.b;
      } else {
        if (this.isLeft()) {
          return x(this.a);
        } else {
          return this.throwIllegalEitherState();
        }
      }
    }

    orElse(x: () => api.Either<A | undefined, B | undefined>): api.Either<A | undefined, B | undefined> {
      return (this.isRight()) ? this : (this.isLeft()) ? x() : (this.throwIllegalEitherState());
    }

    swap() {
      return newEither(this.b, this.a);
    }

    match<C, D>(m: api.EitherMatcher<A, B, C, D>) {
      return (this.isLeft()) ? m.left(this.a) : m.right(this.b);
    }

    tap(tapFunction: (value: (B | undefined)) => void): api.Either<A | undefined, B | undefined> {

      this.match({
        left: () => void 0,
        right: (b: B) =>
          tapFunction(b)
      })
      return this;

    }


    private throwIllegalEitherState = (): never => {
      throw new Error("Illegal Either state !")
    };

  }

  /**
   * This builder function accepts 'undefined' values, but is considered 'safe' at runtime =>
   *  there is no method in the API to directly 'unwrap' a value from the Either instance, so
   *  undefined is not supposed to 'escape'.
   *
   * @param {A | undefined} a
   * @param {B | undefined} b
   * @returns {api.Either<A, B>}
   */
  const newEither = <A, B>(a: A | undefined, b: B | undefined): api.Either<A, B> => {
    return new EitherImpl(a, b) as api.Either<A, B>;
  }

}
