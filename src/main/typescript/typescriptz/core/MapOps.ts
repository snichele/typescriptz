import {api} from "../api/typescriptz-api";
import {Maybe} from "./Maybe";
import {Tuples} from "./Tuples";

/**
 * js 'Map' operations.
 *
 * Basically, provides common functional programming friendly functions for acting on js Maps.
 */
export namespace MapOps {

  /**
   * From a Map<A,B>, return a function to
   * return a Just<B> with the getted (key:A) element wrapped,
   * an Empty<B> if there is no such element.
   * (curryfied form)
   *
   * @param from the Map to maybe get something from.
   * @param key the key to use to retrieve element.
   * @returns {Maybe.Maybe<B>} getted value wrapped in a Maybe.
   */
  export function maybeGet<A, B>(from: Map<A, B> | ReadonlyMap<A, B>): (k: A) => api.Maybe<B> {
    return (key: A) => Maybe.fromNullable(from.get(key))
  }

  export function maybeGetOr<A, B>(from: Map<A, B> | ReadonlyMap<A, B>, defaultValue: () => B): (key: A) => B {
    return (key: A) =>
      maybeGet(from)(key).getOrElse(defaultValue)
  }

}



