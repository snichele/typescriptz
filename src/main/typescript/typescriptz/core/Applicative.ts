import {api} from "../api/typescriptz-api";
import {PointedFunctor} from "./PointedFunctor";
import {Maybe} from "./Maybe";

export class Applicative<A> implements api.Applicative<A> {

  private pointedFunctor: PointedFunctor<A>;
  private _applic: api.Applic;

  constructor(pointedFunctor: PointedFunctor<A>, applic: api.Applic) {
    this.pointedFunctor = pointedFunctor;
    this._applic = applic;
  }

  functor<B>() {
    return {
      fmap: (f: (a: A) => B) => {
        return this.pointedFunctor.fmap(f);
      }
    };
  }

  pointed() {
    return {
      point: (f: () => A) => {
        return this.pointedFunctor.point(f);
      }
    };
  }

  // // any everwhere because typesysctem compilation errors are driving me mad
  fmap<B>(f: (a: A) => B): (ma: any) => any { return this.functor<B>().fmap(f); }

  point(a: () => A): api.Context<A> { return this.pointed().point(a); }

  applic<B>(f: api.Context<(a: A) => B>): (p1: api.Context<A>) => api.Context<B> { return this._applic.applic(f); }

  ap<B>(f: api.Context<(a: A) => B>): (ctx: api.Context<A>) => api.Context<B> { return this._applic.applic(f); }

}

// export namespace Applicative {
//   export const ofMaybe = <T>() => Maybe.Empty<T>().asApplicative();
// }
//
