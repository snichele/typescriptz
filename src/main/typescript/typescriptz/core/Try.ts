import {Either} from "./Either";
import {api} from "../api/typescriptz-api";


export namespace Try {

  export const from = <A>(mayThrows: () => A): api.Either<Error, A> => {
    try {
      return Either.Right(mayThrows());
    } catch (e) {
      return Either.Left(e);
    }
  }

}