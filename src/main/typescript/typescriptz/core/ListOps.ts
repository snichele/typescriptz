import {api} from "../api/typescriptz-api";
import {Maybe} from "./Maybe";
import {Tuples} from "./Tuples";

/**
 * Lists operations.
 *
 * Basically, provides common functional programming lists functions for acting on js arrays.
 */
export namespace ListOps {

  import Tuple2 = api.Tuple2;

  /**
   * Return a Just with the first array element wrapped, an Empty if there is no such element.
   *
   * @param as the array to maybe extract head from.
   * @returns {Maybe.Maybe<A>} head wrapped in a Maybe.
   */
  export function maybeHead<A>(as: Array<A>): api.Maybe<A> {
    const headArray: Array<A> = (as == null) ? [] : as.slice(0, 1);
    return (headArray.length === 0) ? Maybe.Empty<A>() : Maybe.Just(headArray[0]);
  }

  /**
   * Returns an array formed from the first array and a second array by combining corresponding elements in pairs.
   * If one of the two collections is longer than the other, its remaining elements are ignored.
   *
   * @param firstArray
   * @param secondArray
   */
  export function zip<A, B>(firstArray: Array<A>, secondArray: Array<B>): Array<api.Tuple2<A, B>> {
    return <Array<api.Tuple2<A, B>>> (firstArray.map((v: A, idx: number) => {
      return (secondArray[idx]) ? Tuples.Tuple2(v, secondArray[idx]) : void 0;
    }).filter((tuple: api.Tuple2<A, B> | undefined) => tuple !== void 0));
  }

  /**
   * Execute the lambda (toExecuteIfEmpty, toExecuteIfOne, toExecuteIfMoreThanOne)
   * corresponding to the length of the array.
   *
   * @param array
   * @param toExecuteIfEmpty
   * @param toExecuteIfOne
   * @param toExecuteIfMoreThanOne
   * @return {B}
   */
  export function lengthMatcher<A, B>(array: Array<A>,
                                      toExecuteIfEmpty: () => B,
                                      toExecuteIfOne: (as: Array<A>) => B,
                                      toExecuteIfMoreThanOne: (ass: Array<A>) => B): B {
    return (array.length === 0) ? toExecuteIfEmpty() :
      (array.length === 1) ? toExecuteIfOne(array) : toExecuteIfMoreThanOne(array);
  }

  /**
   * Will group the following array of A, using the 'extractorFunction' to get
   * the key value from each A to use for grouping.
   * Will return an array of Tuple2<B, Array<A>>.
   * Example :
   *
   * class Toto {
   *    static xValue = (t: Toto) => t.x;
   *    constructor(public x: string, public y: number) { }
   * }
   *
   *   const ts = [new Toto("a", 10), new Toto("a", 12), new Toto("b", 15)];
   *   const result: Tuple2<string, Array<Toto>> = ListOps.groupBy(ts, Toto.xValue);
   *
   * @param array
   * @param extractorFunction
   * @returns {A|Array<Tuple2<any, Array<any>>>}
   */
  export function groupBy<A, B>(array: Array<A>, extractorFunction: (a: A) => B): Array<Tuple2<B, Array<A>>> {
    return array.reduce((memo: Array<Tuple2<B, Array<A>>>, a: A) => {
      return ListOps.maybeHead(
        memo.filter((_: Tuple2<B, Array<A>>) =>
          _.first() === extractorFunction(a)
        )
      ).match({
        empty: () => {
          return memo.concat([Tuples.Tuple2(extractorFunction(a), [a])]);
        },
        just: (existingTuple: Tuple2<B, Array<A>>) => {
          return memo.filter((_: Tuple2<B, Array<A>>) => _ !== existingTuple).concat([
            Tuples.Tuple2(extractorFunction(a), existingTuple.second().concat([a]))
          ]);
        }
      });
    }, []);
  }

  /**
   * Maybe return the only element of an Array, IIF this array, after filtering
   * with the predicate 'f', has only one element.
   * Otherwise, return Maybe.Empty for all other cases (array empty, array with more than one element...)
   */
  export function onlyOneThatMatch<T>(ts: Array<T>, match: (t: T) => boolean) {
    return Maybe.Just<Array<T>>(
      ts.filter(_ => match(_)) // Take all that match
    ).filter(_ =>
      _.length === 1  // Only one should match, otherwhise, it's KO !
    ).map(_ =>
      _[0]  // Get the only one item.
    );
  }

}



