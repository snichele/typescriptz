import {api} from "../api/typescriptz-api";
import {ListOps} from "./ListOps";

/**
 * api.Eq related operations.
 *
 * @see {@link api.Eq}
 */
export namespace Eq {

  /**
   * Two WithCode concrete instances are considered equals if :
   * - they have the same constructor
   * - they have the same 'code' property value.
   */
  export class WithCodeEq implements api.Eq {

    code: string;

    constructor(code: string) {
      this.code = code;
    }

    equals(another: api.Eq): boolean {
      return (another instanceof WithCodeEq) ? another.code === this.code : false;
    }

  }

  export namespace WithCodeEq {

    export const areEquals = (x: WithCodeEq) => (y: WithCodeEq) => x.equals(y);

    export const isPairEquals = (pair: [WithCodeEq, WithCodeEq]) => pair[0].equals(pair[1]);

    export const getCode = (x: WithCodeEq) => x.code;

    export const findInArray = (code: string) => <T extends WithCodeEq>(xs: Array<T>): api.Maybe<T> =>
      ListOps.maybeHead(xs.filter(_ => _.code === code));

    export const of = (s: string) => new WithCodeEq(s);

  }

}
