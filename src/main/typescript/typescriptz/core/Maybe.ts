import {api} from "../api/typescriptz-api";
import {Applicative} from "./Applicative";
import {FunctionsOps} from "./FunctionsOps";
import {PointedFunctor} from "./PointedFunctor";


/**
 * api.Maybe related operations.
 *
 * @see {@link api.Maybe}
 */
export namespace Maybe {

  // --- EXPORTS
  /** Wrap a value in Just, or return Empty if the value is null / undefined */
  export function fromNullable<A>(a: A | undefined | null): api.Maybe<A> {
    return emptyIf(api.Predicates.nullOrEmptyPredicate, a);
  }

  /** Return a Just containing the first array element if it exists,
   * or return Empty if the array is empty. */
  export function headOption<A>(as: Array<A>): api.Maybe<A> {
    return (as[0]) ? Just(as[0]) : Empty<A>();
  }

  export function emptyIf<A>(predicate: (someA: A | undefined | null) => boolean, a: A | undefined | null): api.Maybe<A> {
    return (predicate(a)) ? Empty<A>() : Just(<A>a); // typecast to A because we are 'safe' here !
  }

  /** Return the unique Empty instance. */
  export function Empty<A>(): api.Maybe<A> {
    return emptyInstance;
  }

  /** Wrap the provided value in a Just. */
  export function Just<A>(a: A): api.Maybe<A> {
    return new Just_(a);
  }

  // --- PRIVATE

  /**
   * Private implementation of Maybe.
   **/

  /* tslint:disable:no-shadowed-variable */
  class Maybe<A> implements api.Maybe<A> {

    /** Catamorphism.
     * Run the given function on the underlying value if present, otherwise return
     * the provided fallback value */
    cata<B>(f: (a: A) => B, b: () => B): B {
      if ((<any>this) instanceof Empty_) {
        return b();
      }
      const just: any = this; // shitty hack to workaround compiler crazyness
      return f((<Just_<A>>just).a);
    }

    /** @inheritDoc */
    fold<B>(f: (a: A) => B, b: () => B): B {
      return this.cata(f, b);
    }

    /** @inheritDoc */
    exists(predicate: (t: A) => boolean): boolean {
      return this.cata(predicate, () => false);
    }


    filter(predicate: (value: A) => boolean): api.Maybe<A> {
      return this.cata(
        (a: A) => (predicate(a)) ? this : Empty<A>(),
        /* tslint:disable:no-unnecessary-callback-wrapper */
        () => Empty<A>()
      );
    }

    filterNot(predicate: (value: A) => boolean): api.Maybe<A> {
      return this.cata(
        (a: A) => (!predicate(a)) ? this : Empty<A>(),
        () => Empty<A>()
      );
    }

    /** @inheritDoc */
    getOrElse(thunk: () => A): A {
      return this.cata((a: A) => a, thunk);
    }

    /** @inheritDoc */
    orElse(m: Maybe<A>): Maybe<A> {
      return this.cata((a: A) => new Just_(a), () => m);
    }

    /** @inheritDoc */
    get(): A | undefined {
      return this.cata((a: A) => a, () => void 0);
    }

    /** @inheritDoc */
    isJust(): boolean {
      return this.cata(() => true, () => false);
    }

    /** @inheritDoc */
    isDefined(): boolean {
      return this.cata(() => true, () => false);
    }

    /** @inheritDoc */
    isEmpty(): boolean {
      return this.cata(() => false, () => true);
    }

    /** @inheritDoc */
    map<B>(f: (a: A) => B): Maybe<B> {
      /*
       in scalaz
       cata(f andThen just[B], Empty[B] )
       */
      return this.cata(
        FunctionsOps.first(f).then(Just).finish(),
        Empty
      );
    }

    flatMap<B>(f: (a: A) => api.Maybe<B>): Maybe<B> {
      return <Maybe<B>>this.cata(
        f,
        Empty
      );
    }

    /*
     final def map[B](f: A => B): Maybe[B] =
     cata(f andThen just[B], empty[B])

     final def flatMap[B](f: A => Maybe[B]) =
     cata(f, empty[B])
     */

    asApplicative(): api.Applicative<A> {
      return <any> new MaybeApplicative(); // another shitty hack to workaround compiler crazyness
    }

    match<A, B>(m: api.MaybeMatcher<A, B>) {
      return (this.isEmpty()) ?
        <B>m.empty() :
        <B>m.just((<Just_<A>>(<any>this)).a); // another shitty hack to workaround compiler crazyness
    }


    equals(another: api.Maybe<A>,
           comparaisonFunction: (some: A, another: A) => boolean = (some: A, another: A) => some === another): boolean {
      return this.match({
        empty: () => another.isEmpty(),
        just: (a: A) =>
          another.match({
            empty: () => false,
            just: (aa: A) =>
              comparaisonFunction(aa, a)
          }),
      });
    }

    forEach(callbackfn: (value: A) => void): void {
      this.match({
        empty: () => void 0,
        just: (aa: A) =>
          callbackfn(aa)
      })
    }

    tap(tapFunction: (value: A) => void): api.Maybe<A> {
      this.match({
        empty: () => void 0,
        just: (aa: A) =>
          tapFunction(aa)
      })
      return this;
    }



  }

  /* tslint:disable:class-name */
  /** Internal (not exported) instance of Maybe.Empty. */
  class Empty_<A> extends Maybe<A> {
  }

  const emptyInstance = new Empty_<any>();

  /** Internal (not exported) instance of Maybe.Just. */
  class Just_<A> extends Maybe<A> {
    constructor(public a: A) {
      super();
    }
  }

  // Typeclasses
  export class MaybePointed<A> implements api.Pointed<A> {

    point(a: () => A): api.Maybe<A> {
      return Just(a());
    }
  }

  export class MaybeFunctor<A> implements api.Functor<A> {

    // any everwhere because typesysctem compilation errors are driving me mad
    fmap<B>(f: (p1: A) => B): (ma: any) => any {
      return (ama: api.Maybe<A>) => <any> ama.map(f);
    }

  }

  export class MaybePointedFunctor<A> extends PointedFunctor<A> {

    constructor(functor: api.Functor<A>, pointed: api.Pointed<A>) {
      super(functor, pointed);
    }
  }

  export class MaybeApplic implements api.Applic {
// any everwhere because typesysctem compilation errors are driving me mad
    applic<A, B>(mf: api.Maybe<(a: A) => B>): (ma: any) => any {
      return (ama: api.Maybe<A>): api.Maybe<B> => {
        return ama.map(mf.getOrElse(() => (x: any) => x));
      };
    }
  }

  export class MaybeApplicative<T> extends Applicative<T> {

    constructor() {
      super(
        <any>new MaybePointedFunctor(
          new MaybeFunctor(),
          new MaybePointed()
        ),
        new MaybeApplic()
      );
    }
  }

}
