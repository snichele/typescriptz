import {api} from "../api/typescriptz-api";
import {ListOps} from "./ListOps";
import {Maybe} from "./Maybe";


/**
 * api.Set related operations.
 *
 * @see {@link api.Set}
 */
export namespace Set {

  type Eq = api.Eq;
  import EqOrPrimitive = api.EqOrPrimitive;

  /**
   * Smart constructor for generating a set from a TS array.
   * The original array will be cloned and deduplicated.
   *
   * /!\ It will deduplicate the array content by retaining the first occurence
   * of each duplicated element !
   *
   * */
  export function fromArray<A extends EqOrPrimitive>(array: Array<A>): api.Set<A> {
    return new Set_(array);
  }

  const checkEqualityOrEqOrPrimitive = <A>(a: A) => (_: EqOrPrimitive) => {
    if (typeof _ === "number" || typeof _ === "string") {
      return (<any>_) === a;
    } else {
      if ((<any>_)["equals"]) { // instanceof Eq does not work, compiler error ???
        return _.equals(<any>a);
      } else {
        /* tslint:disable:triple-equals */
        return (<any>_) == <any>a;
        /* tslint:enable:triple-equals */
      }
    }
  };

  const checkNonEqualityOrEqOrPrimitive = <A>(a: A) => (_: EqOrPrimitive) => {
    return !checkEqualityOrEqOrPrimitive(a)(_);
  };

  const arrayAlreadyContainsItem = (array: any, item: any): boolean => {
    return array.filter(checkEqualityOrEqOrPrimitive(item)).length !== 0;
  };

  /* tslint:disable:class-name */
  export class Set_<A extends EqOrPrimitive> implements api.Set<A> {

    private _underlyingArray: Array<A>;

    constructor(arrayToDeduplicateFirst: Array<A>) {
      // check for duplicates !
      this._underlyingArray = arrayToDeduplicateFirst.reduce((memo: Array<A>, next: A) => {
        return (arrayAlreadyContainsItem(memo, next)) ? memo : memo.concat([next]);
      }, []);
    }

    get length() {
      return this._underlyingArray.length;
    }

    toArray(): Array<A> {
      return this._underlyingArray.slice(0); // defensive copying !
    }

    add(a: A): api.Set<A> {
      if (arrayAlreadyContainsItem(this._underlyingArray, a)) {
        return this;
      } else {
        return new Set_(this.toArray().concat([a]));
      }
    }

    remove(a: A): api.Set<A> {
      return new Set_(this.toArray().filter(checkNonEqualityOrEqOrPrimitive(a)));
    }

    union(s: api.Set<A>): api.Set<A> {
      return s.toArray().reduce((_1: api.Set<A>, _2: A) => {
        return _1.add(_2);
      }, <api.Set<A>>this);
    }

    join(separator?: string): string {
      return this._underlyingArray.join(separator);
    }

    reverse(): api.Set<A> {
      return new Set_(this._underlyingArray.slice(0).reverse()); // duplicate to avoid side effects !
    }

    slice(start?: number, end?: number): api.Set<A> {
      return new Set_(this._underlyingArray.slice(start, end));
    }

    sort(compareFn?: (a: A, b: A) => number): api.Set<A> {
      return new Set_(this._underlyingArray.slice(0).sort(compareFn));
    }

    indexOf(searchElement: A, fromIndex?: number): number {
      if (typeof searchElement === "number" || typeof searchElement === "string") {
        return this._underlyingArray.indexOf(searchElement, fromIndex);
      } else {
        if ((<any>searchElement)["equals"]) { // instanceof Eq does not work, compiler error ???
          type NumberTuple = [number, number];
          const initialValue: NumberTuple = [0, -1];
          return (this._underlyingArray.reduce((memo: NumberTuple, elt: A) => {
            if ((<any>elt).equals(<any>searchElement)) { // I'm hacking all the way... :(
              return <NumberTuple>[memo[0] + 1, memo[0]]; // store the index found
            }
            return <NumberTuple>[memo[0] + 1, memo[1]];
          }, initialValue))[1];

        } else {
          return this._underlyingArray.indexOf(searchElement, fromIndex);
        }
      }
    }

    every(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): boolean {
      return this._underlyingArray.every(callbackfn);
    }

    some(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): boolean {
      return this._underlyingArray.some(callbackfn);
    }

    forEach(callbackfn: (value: A, index: number, array: A[]) => void, thisArg?: any): void {
      this._underlyingArray.forEach(callbackfn);
    }

    find(p: (a: A) => boolean): api.Maybe<A> {
      return ListOps.maybeHead(this._underlyingArray.filter(p));
    }

    map<U extends EqOrPrimitive>(callbackfn: (value: A, index: number, array: A[]) => U, thisArg?: any): api.Set<U> {
      return new Set_(this._underlyingArray.map(callbackfn));
    }

    filter(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): api.Set<A> {
      return new Set_(this._underlyingArray.filter(callbackfn));
    }

    filterNot(callbackfn: (value: A, index: number, array: A[]) => boolean, thisArg?: any): api.Set<A> {
      return new Set_(this._underlyingArray.filter((value: A, index: number, array: A[]) =>
        !callbackfn(value, index, array)
      ));
    }

    reduce<U>(callbackfn: (previousValue: U, currentValue: A, currentIndex: number, array: A[]) => U, initialValue: U): U {
      return this._underlyingArray.reduce(callbackfn, initialValue);
    }

    reduceRight<U>(callbackfn: (previousValue: U, currentValue: A, currentIndex: number, array: A[]) => U, initialValue: U): U {
      return this._underlyingArray.reduceRight(callbackfn, initialValue);
    }

    head(): api.Maybe<A> {
      return Maybe.fromNullable(this._underlyingArray[0]);
    }

    tail(): api.Set<A> {
      return Set.fromArray(this._underlyingArray.slice(1));
    }

    last(): api.Maybe<A> {
      return Maybe.fromNullable(this._underlyingArray[this._underlyingArray.length - 1]);
    }

  }

  /* tslint:enable:class-name */
}
