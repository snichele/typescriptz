import {api} from "../api/typescriptz-api";


/**
 * api.Tuples related operations.
 *
 * @see {@link api.Tuples}
 */
export namespace Tuples {

  export function Tuple2<A, B>(a: A, b: B): api.Tuple2<A, B> {
    return new Tuple2_(a, b);
  }

  /* tslint:disable:class-name */
  export class Tuple2_<A, B> implements api.Tuple2<A, B> {

    private _first: A;
    private _second: B;

    constructor(first: A, second: B) {
      this._first = first;
      this._second = second;
    }

    first(): A { return this._first; }

    second(): B { return this._second; }

    bimap<C, D>(firstF: (a: A) => C, secondF: (b: B) => D): api.Tuple2<C, D> {
      return new Tuple2_<C, D>(
        firstF(this._first),
        secondF(this._second)
      );
    }
  }
  /* tslint:enable:class-name */
}
