/**
 * Operations on functions.
 * @Deprecated use Fonction1
 **/

export namespace FunctionsOps {

  export interface ComposeDSL {
    then(f: Function): ComposeDSL;

    andThen(f: Function): ComposeDSL;

    finish(): (a: any) => any;
  }

  /**
   * @Deprecated use Fonction1
   *
   * Build a context for function composition chaining with a nice DSL.
   *
   *   first(f1).then(f2).andThen(f3)...
   *
   * finish with finish(), and get back your final function to execute !
   *
   *   const composedFunction = first(f1).then(f2).andThen(f3).finish();
   *
   * @param f the first function of the composition
   * @returns {typescriptz.core.FunctionsOps.ComposeDSL}
   */
  export function first(f: (args: any) => any): ComposeDSL {
    const c: Composer = new Composer();
    c.then(f);
    return c;
  }

  class Composer implements ComposeDSL {

    private functions: Array<Function> = [];

    then(f: Function): FunctionsOps.ComposeDSL {
      this.functions.push(f);
      return this;
    }

    andThen(f: Function): FunctionsOps.ComposeDSL {
      this.functions.push(f);
      return this;
    }

    finish(): (a: any) => any {
      return this.functions.reduce((memo: (x?: any) => any, next: any) => {
        return memo(next);
      }, doCompose)();
    }
  }

  /* tslint:disable:typedef */

  /* Based on http://scott.sauyet.com/Javascript/Talk/Compose/2013-05-22/ */
  function doCompose(f: any) {
    const queue = f ? [f] : [];
    return function fn(g: any): any {
      if (arguments.length) {
        queue.push(g);
        return fn;
      }
      return function () {
        let args = Array.prototype.slice.call(arguments);
        queue.forEach(function (func) {
          args = [func.apply(this as any, args)];
        });
        return args[0];
      };
    };
  }

}



