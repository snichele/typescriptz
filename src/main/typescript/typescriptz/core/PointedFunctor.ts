import {api} from "../api/typescriptz-api";

export class PointedFunctor<A> {

  private functor: api.Functor<A>;
  private pointed: api.Pointed<A>;

  constructor(functor: api.Functor<A>, pointed: api.Pointed<A>) {
    this.functor = functor;
    this.pointed = pointed;
  }

  point(a: () => A): api.Context<A> {
    return this.pointed.point(a);
  }

  fmap<B>(f: (a: A) => B): (c: api.Context<A>) => api.Context<B> {
    return this.functor.fmap(f);
  }
}
