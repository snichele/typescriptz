import {api} from "../api/typescriptz-api";
import {Maybe} from "./Maybe";
import {Tuples} from "./Tuples";

/**
 * Strings related functions.
 *
 * Because StringUtils is so 1995 !
 */
export namespace StringOps {

  export const longerThan =
    (l: number) =>
      (s: string): boolean =>
        s.length > l;

  export const isEmpty =
    (str: string): boolean =>
      str === null || str === undefined ? true : /^[\s\xa0]*$/.test(str);

  export const isNotEmptyOrEmptyString =
    (str: string): boolean =>
      !isEmpty(str);

  export const between =
    (start: number, end?: number) =>
      (s: string) =>
        s.substr(start, end);

  export const textBeforeToken =
    (token: string) =>
      (str: string): string =>
        between(0, str.indexOf(token))(str);

  export const before =
    (position: number) =>
      between(0, position);

  export const after =
    (position: number) =>
      between(position, void 0);

  export const capitalize =
    (str: string): string =>
      str.substr(0, 1).toUpperCase() + str.substring(1).toLowerCase();

  export const bindTemplate =
    (template: string, values: Object) =>
      template.replace(/\${(.*?)}/g, (match: string, p1: string) => (<any>values)[p1]);

}



