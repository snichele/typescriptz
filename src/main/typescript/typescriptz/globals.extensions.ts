import {api} from "./api/typescriptz-api";
import {Maybe} from "./core/Maybe";

export {};

declare global {

  interface String {
    just: () => api.Maybe<string>;
  }

  interface Boolean {
    just: () => api.Maybe<boolean>;
  }

  interface Number {
    just: () => api.Maybe<number>;
  }

  interface StringConstructor {
    fromNullable: (val: any) => api.Maybe<string>;
  }

  interface BooleanConstructor {
    fromNullable: (val: any) => api.Maybe<boolean>;
  }

  interface NumberConstructor {
    fromNullable: (val: any) => api.Maybe<number>;
  }

}


String.prototype.just = function (): api.Maybe<string> {
  return Maybe.fromNullable(this);
};

Boolean.prototype.just = function (): api.Maybe<boolean> {
  return Maybe.fromNullable(this);
};

Number.prototype.just = function (): api.Maybe<number> {
  return Maybe.fromNullable(this);
};

String.fromNullable = function (val: any): api.Maybe<string> {
  return Maybe.fromNullable(val);
};

Boolean.fromNullable = function (val: any): api.Maybe<boolean> {
  return Maybe.fromNullable(val);
};

Number.fromNullable = function (val: any): api.Maybe<number> {
  return Maybe.fromNullable(val);
};
